function updatePlot(fh,h,rho,v,n,p,c,t,M)

set(h{1},'YData',p);
set(h{1,2},'YData',M.p_data_bottom(t));
set(h{1,3},'YData',M.p_data_top(t));

set(h{2},'YData',n);
set(h{2,2},'YData',M.n_data_bottom(t));
set(h{2,3},'YData',M.n_data_top(t));

set(h{3},'YData',rho);
set(h{4},'YData',v);
set(h{5},'YData',c);

figure(fh);
title(['t =' num2str(t)]);
drawnow;