function fig_handles = plotSolution(fHandle,x,t,M,q1,q2,q3,FORM,BC)

if(nargin <= 8); BC = true; end;
if isempty(fHandle); fHandle = figure(); end;

if( strcmp(FORM,'cons') || strcmp(FORM,'prim') )
    rho = q1; v = q2./q1; n = q3./q1;
else
    rho = q1; v = q2; n = q3;
end
p = p_func(rho,n,M);

figure(fHandle);
fig_handles = cell(5,3);

%%% SUBPLOTS %%%
subplot(5,1,1);
fig_handles{1} = plot(x,p,'b-o');
ylabel('p')
hold on;
if(BC)
    fig_handles{1,2} = plot(x(1),M.p_data_bottom(t),'rx');
    fig_handles{1,3} = plot(x(end),M.p_data_top(t),'rx');
end
hold off;

subplot(5,1,2)
fig_handles{2} = plot(x,n,'b-o');
ylabel('n')
hold on;
if(BC)
    fig_handles{2,2} = plot(x(1),M.n_data_bottom(t),'rx');
    fig_handles{2,3} = plot(x(end),M.n_data_top(t),'rx');
end
hold off;

subplot(5,1,3);
fig_handles{3} = plot(x,rho,'b-o');
ylabel('rho')

subplot(5,1,4);
fig_handles{4} = plot(x,v,'b-o');
ylabel('v')

subplot(5,1,5);
c = sound_speed(rho,n,M);
fig_handles{5} = plot(x,c,'b-o');
ylabel('c')

title(['t = ' num2str(t)])

drawnow;