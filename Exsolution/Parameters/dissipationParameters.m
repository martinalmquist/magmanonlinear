function [C_AD,C_AD_boundary,C_upwind,C_upwind_boundary]...
    = dissipationParameters(order,FORM,AD)

%%% Artificial damping parameters, tuned with tau = 0.1 %%%
if(order == 8) 
    C_upwind = 1/50; C_upwind_boundary = 1/4000; 
    C_AD = 6*[0,1e-1,1e-1]; C_AD_boundary = 2e-1*[0,1e-1,1e-1];
%     C_AD = 10*6*[1e-2,1e-1,1e-1]; C_AD_boundary = 10*2e-1*[1e-2,1e-1,1e-1];
    if(strcmp(FORM,'prim_rho_v_n'))
        C_AD = 50*[1e-1,2e-1,1e-1]; C_AD_boundary = 0.25*[1e-1,2e-1,1e-1];
    end
elseif(order == 6); 
    C_upwind = 1/25; C_upwind_boundary = 0; 
    C_AD = 50*[0,1e-1,1e-1]; C_AD_boundary = [0,0,0];
    if(strcmp(FORM,'prim_rho_v_n'))
        C_AD = 500*[1e-1,1e-1,1e-1];
    end
elseif(order == 4); 
    C_upwind = 1/10; C_upwind_boundary = 0; 
    C_AD = 100*[0,1e-1,1e-1]; C_AD_boundary = [0,0,0];
    if(strcmp(FORM,'prim_rho_v_n'))
        C_AD = 300*[1e-1,1e-1,1e-1];
    end
elseif(order == 2)
    C_upwind = 1/10; C_upwind_boundary = 0; 
     C_AD = 400*[0,1e-1,1e-1]; C_AD_boundary = [0,0,0];
    if(strcmp(FORM,'prim_rho_v_n'))
        C_AD = 500*[1e-1,1e-1,1e-1];
    end
else
    error('That order is not available');
end

if(~AD); C_AD = [0,0,0]; C_AD_boundary = [0,0,0]; end