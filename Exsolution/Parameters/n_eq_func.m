function n_eq = n_eq_func(p,M)
    % Equilibrium gas mass fraction as a function of pressure
    n_eq = max(M.n0 - M.s*sqrt(p),0);
    
    % Smoother version
%     n_eq = max(M.n0./sqrt(M.p0).*(p-M.p0).^2./(p+M.p0).^(1+1/2),0);
end