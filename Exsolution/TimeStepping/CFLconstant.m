function CFL = CFLconstant(order)

if(order==2); CFL = 0.5;
elseif(order==4); CFL = 0.4; 
elseif(order==6); CFL = 0.3; 
elseif(order==8); CFL = 0.2;
else error('That order is not available');
end