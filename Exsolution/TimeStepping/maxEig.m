function Lambda_max = maxEig(rho,v,n,M)

    % Similar transformation R, and inv(R): Note that A=R*Lambda*RI
    k0 = M.k0;
    R = M.R; T = M.T;
    rho_l0 = M.rho_l0; p0 = M.p0; 
      
    a = 1./(R.*T);
    b = rho_l0.*(1-p0./k0);
    c = rho_l0./k0;
    
    n_gt_0 = n > 1e-12;
    n_0 = n<=1e-12;
    
    rho0 = rho(n_0); v0 = v(n_0);
    rho = rho(n_gt_0); v = v(n_gt_0); n = n(n_gt_0);
    % For n = 0
    t2 = 1.0./c.^2;
    t3 = b-rho0;
    t4 = t3.^2;
    t5 = t2.*t4;
    t6 = 1.0./sqrt(t5);
    t7 = b.^2;
    t8 = rho0.^2;
    t9 = t7+t8-b.*rho0.*2.0;
    t10 = t2.*t9;
    t11 = sqrt(t10);
    t12 = c.*t11;
    t13 = -b+rho0+t12;
    t14 = sqrt(c.*t3.*t13.*-2.0);
    Lambda_max_n0 = max(max(abs([v0,v0-t2.*t6.*t14.*(1.0./2.0),v0+t2.*t6.*t14.*(1.0./2.0)])));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % For n>0
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    t2 = 1.0./c.^2;
        t3 = rho.^2;
        t4 = 1.0./a.^2;
        t5 = n.^2;
        t6 = 1.0./a;
        t7 = 1.0./c;
        t8 = a.^2;
        t9 = t3.^2;
        t10 = t8.^2;
        t11 = b.^2;
        t12 = c.^2;
        t13 = t2.*t11;
        t14 = t2.*t3;
        t15 = t3.*t4.*t5;
        t16 = t2.*t3.*t5;
        t17 = n.*t3.*t6.*t7.*2.0;
        t18 = b.*n.*rho.*t2.*2.0;
        t19 = b.*n.*rho.*t6.*t7.*2.0;
        t22 = n.*t2.*t3.*2.0;
        t23 = b.*rho.*t2.*2.0;
        t24 = t3.*t5.*t6.*t7.*2.0;
        t20 = t13+t14+t15+t16+t17+t18+t19-t22-t23-t24;
        t21 = t12.^2;
        t25 = sqrt(t20);
        t26 = sqrt(2.0);
        t27 = 1.0./rho.^2;
        t28 = 1.0./sqrt(t20);
        t29 = c.*t3.*t9.*t10;
        t30 = c.*t9.*t10.*t11;
        t31 = a.*n.*t3.*t8.*t9.*t12.*3.0;
        t32 = c.*t3.*t5.*t9.*t10.*3.0;
        t33 = a.*n.*t3.*t5.*t9.*t21;
        t34 = rho.*t9.*t10.*t12.*t25;
        t35 = c.*t3.*t5.*t8.*t9.*t12.*3.0;
        t36 = a.*n.*t3.*t5.*t8.*t9.*t12.*3.0;
        t37 = rho.*t5.*t8.*t9.*t21.*t25;
        t38 = rho.*t5.*t9.*t10.*t12.*t25;
        t39 = b.*c.*n.*rho.*t9.*t10.*4.0;
        t40 = a.*c.*n.*rho.*t8.*t9.*t12.*t25.*2.0;
        t41 = a.*n.*t8.*t9.*t11.*t12;
        t42 = b.*c.*rho.*t5.*t8.*t9.*t12.*2.0;
        t43 = a.*b.*c.*n.*t8.*t9.*t12.*t25;
        t44 = b.*n.*t9.*t10.*t12.*t25;
        t45 = t29+t30+t31+t32+t33+t34+t35+t36+t37+t38+t39+t40+t41+t42+t43+t44-b.*c.*rho.*t9.*t10.*2.0-c.*n.*t3.*t9.*t10.*3.0-c.*n.*t9.*t10.*t11-b.*t9.*t10.*t12.*t25-c.*n.*t3.*t5.*t9.*t10-a.*t3.*t5.*t8.*t9.*t12.*6.0-n.*rho.*t9.*t10.*t12.*t25.*2.0-b.*c.*rho.*t5.*t9.*t10.*2.0-c.*n.*t3.*t5.*t8.*t9.*t12.*3.0-a.*c.*rho.*t5.*t8.*t9.*t12.*t25.*2.0;
        t46 = sqrt(t45);
        Lambda_max = max(max(abs([v,...
            v-t2.*t4.*t26.*t27.*t28.*t46.*(1.0./2.0),...
            v+t2.*t4.*t26.*t27.*t28.*t46.*(1.0./2.0)])));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        Lambda_max = max(max([Lambda_max;Lambda_max_n0]));
    
end