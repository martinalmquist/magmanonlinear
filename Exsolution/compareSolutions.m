function compareSolutions(out1,out2)

t = out1.t;
x1 = out1.x; x2 = out2.x;
rho1 = out1.rho; v1 = out1.v; n1 = out1.n; p1 = out1.p;
rho2 = out2.rho; v2 = out2.v; n2 = out2.n; p2 = out2.p;

close all;

for i = 1:30:length(t)
    
    %%% SUBPLOTS %%%
    subplot(4,1,1)
    plot(x1,p1(:,i),'bo')
    hold on;
    plot(x2,p2(:,i),'rx')
    ylabel('p')
    hold off;
    
    subplot(4,1,2)
    plot(x1,n1(:,i),'bo')
    hold on;
    plot(x2,n2(:,i),'rx')
    ylabel('n')
    hold off;
    
    subplot(4,1,3)
    plot(x1,rho1(:,i),'bo')
    hold on;
    plot(x2,rho2(:,i),'rx')
    ylabel('rho')
    hold off;
    
    subplot(4,1,4)
    plot(x1,v1(:,i),'bo')
    hold on;
    plot(x2,v2(:,i),'rx')
    ylabel('v')
    hold off;
    
    title(['t = ' num2str(t(i))])
    drawnow;
    
end