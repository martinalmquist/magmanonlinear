function SAT = BC_bottom_cons(t,rho,v,n,q1,q2,q3,M,Hinv)
% Boundary conditions for the bottom boundary, conservative variables.

% Build rotation matrices
if(n(1) > 1e-12)
    [R,Rinv,Lambda_plus] = rotationMatrix_J(rho(1),v(1),n(1)...
        ,M,'plus');
else
    [R,Rinv,Lambda_plus] = rotationMatrix_J_n0(rho(1),v(1)...
        ,M,'plus');
end

R11 = Rinv(1,1); R12 = Rinv(1,2); R13 = Rinv(1,3); 
R21 = Rinv(2,1); R22 = Rinv(2,2); R23 = Rinv(2,3); 
R31 = Rinv(3,1); R32 = Rinv(3,2); R33 = Rinv(3,3); 

%%%%%%%%%%%%%%%%%%%%%% OSSIAN'S NEW TECHNIQUE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-------- Data for rho and n. If outflow, only data for rho is used. ------------------%
if( strcmp(M.BC_bottom,'rho_n') && strcmp(M.BC_method_bottom,'new') )
    rho_data = M.rho_data_bottom(t);
    n_data = M.n_data_bottom(t);
    V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
    if(v(1)>0)
        % This is an inflow
        % w2 is outgoing
        w2 = R21*rho(1) + R22*rho(1)*v(1) + R23*rho(1)*n(1);
        w1 = (R11-R21*R12/R22)*rho_data + (R13-R23*R12/R22)*rho_data*n_data + R12/R22*w2;
        w3 = (R31-R21*R32/R22)*rho_data + (R33-R23*R32/R22)*rho_data*n_data + R32/R22*w2;
        
        % Rotate to characteristic variables in SAT term
        V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
        W = [w1;w2;w3];
        SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    else
        % This is an outflow. Ignore data for n.
        % w3 is ingoing
        W = Rinv*V;
        w1 = W(1); w2 = W(2);
        a_vec = R'*[1;0;0]; a1 = a_vec(1); a2 = a_vec(2); a3 = a_vec(3);
        w3 = 1/a3*(rho_data - a1*w1 - a2*w2);
        
        % Rotate to characteristic variables in SAT term
        W = [w1;w2;w3];
        SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    end
end
%--------------------------------------------------------%

%-------- Data for p and n. If outflow, only data for p is used. ------------------%
if( strcmp(M.BC_bottom,'p_n') && strcmp(M.BC_method_bottom,'new') )
    p_data = M.p_data_bottom(t);
    n_data = M.n_data_bottom(t);
    rho_data = rho_func(n_data,p_data,M); % Eq of state
    V = [rho(1); rho(1)*v(1); rho(1)*n(1)];
    if(v(1)>0)
        % This is an inflow
        % w2 is outgoing
        w2 = R21*rho(1) + R22*rho(1)*v(1) + R23*rho(1)*n(1);
        w1 = (R11-R21*R12/R22)*rho_data + (R13-R23*R12/R22)*rho_data*n_data + R12/R22*w2;
        w3 = (R31-R21*R32/R22)*rho_data + (R33-R23*R32/R22)*rho_data*n_data + R32/R22*w2;
        
        % Rotate to characteristic variables in SAT term
        V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
        W = [w1;w2;w3];
        SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    else
        % This is an outflow. Ignore data for n.
        n_data = n(1);
        rho_data = rho_func(n_data,p_data,M); % Eq of state
        
        % w3 is ingoing
        W = Rinv*V;
        w1 = W(1); w2 = W(2);
        a_vec = R'*[1;0;0]; a1 = a_vec(1); a2 = a_vec(2); a3 = a_vec(3);
        w3 = 1/a3*(rho_data - a1*w1 - a2*w2);
        
        % Rotate to characteristic variables in SAT term
        W = [w1;w2;w3];
        SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    end
end
%--------------------------------------------------------%

%-------- Wall boundary, velocity = 0. ------------------%
% Martin is not 100% sure about what we can do with w2, which 
% is moving with speed 0, but Ossian is confident that we can set
% w3 = a*w1 + b*w2 anyway.
if( strcmp(M.BC_bottom,'wall') && strcmp(M.BC_method_bottom,'new') )
    % w3 is ingoing
    % w2 is outgoing
    % w1 has speed 0.
    
    v_data = 0;
    
    V = [rho(1); rho(1)*v(1); rho(1)*n(1)];
    W = Rinv*V;
    w1 = W(1); w2 = W(2);
    a_vec = R'*[0;1/rho(1);0]; a1 = a_vec(1); a2 = a_vec(2); a3 = a_vec(3);
    w3 = 1/a3*(v_data - a1*w1 - a2*w2);

    % Rotate to characteristic variables in SAT term
    W = [w1;w2;w3];
    SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    
end
%--------------------------------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%% KEN'S AND MARTINS'S OLD TECHNIQUE %%%%%%%%%%%%%%%%%%
%-------- Data for rho and n. If outflow, only data for rho is used. ------------------%
if( strcmp(M.BC_bottom,'rho_n') && strcmp(M.BC_method_bottom,'old') )
    rho_data = M.rho_data_bottom(t);
    n_data = M.n_data_bottom(t);
    v_data = v(1);
    if(v(1)>0)
        % This is an inflow
    else
        % This is an outflow. Ignore data for n.
        n_data = n(1);
    end
    
    % Rotate to characteristic variables in SAT term
    V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
    g = [rho_data;rho_data*v_data;rho_data*n_data];
    SAT = -1*Hinv*R*Lambda_plus*Rinv*(V-g);
end
%--------------------------------------------------------%

%-------- Data for p and n. If outflow, only data for p is used. ------------------%
if( strcmp(M.BC_bottom,'p_n') && strcmp(M.BC_method_bottom,'old') )
    p_data = M.p_data_bottom(t);
    n_data = M.n_data_bottom(t);
    rho_data = rho_func(n_data,p_data,M);
    v_data = v(1);
    if(v(1)>0)
        % This is an inflow
    else
        % This is an outflow. Ignore data for n.
        n_data = n(1);
        rho_data = rho_func(n_data,p_data,M);
    end
    
    % Rotate to characteristic variables in SAT term
    V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
    g = [rho_data;rho_data*v_data;rho_data*n_data];
    SAT = -1*Hinv*R*Lambda_plus*Rinv*(V-g);
end
%--------------------------------------------------------%

%-------- Wall boundary, velocity = 0. ------------------%
if( strcmp(M.BC_bottom,'wall') && strcmp(M.BC_method_bottom,'old') )
    
    n_hat = n(1); rho_hat = rho(1); v_hat = -v(1);
    
    % Specify BC in terms of conservative variables
    q1_BC = rho_hat; q2_BC = rho_hat*v_hat; q3_BC = rho_hat*n_hat;
    g = [q1_BC; q2_BC; q3_BC];
    V = [q1(1); q2(1); q3(1) ];
    
    % Rotate to characteristic variables in SAT term
    SAT = -1*Hinv*R*Lambda_plus*Rinv*(V-g);
    
end
%--------------------------------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Rotation matrices from Maple.
function [R,Rinv,Lambda_sign] = rotationMatrix_J(rho,v,n,M,sign)
        
        % Similar transformation R, and inv(R): Note that A=R*Lambda*RI
        
        k0 = M.k0;
        R = M.R; T = M.T;
        rho0 = M.rho_l0; p0 = M.p0;
        
        a = 1./(R.*T);
        b = rho0.*(1-p0./k0);
        c = rho0./k0;
        
        % Transformation matrices
        %R
        %    R = R(A,B,C,N,RHO,V)
        
        %    This function was generated by the Symbolic Math Toolbox version 6.1.
        %    12-Jan-2015 13:06:49
        
        t2 = 1.0./a;
        t3 = a.^2;
        t4 = rho.^2;
        t5 = n.^2;
        t6 = 1.0./rho;
        t7 = c.^2;
        t8 = a.*b;
        t9 = 1.0./a.^2;
        t10 = 1.0./c.^2;
        t11 = b.^2;
        t12 = t3.*t11;
        t13 = t3.*t4;
        t14 = t3.*t4.*t5;
        t15 = t4.*t5.*t7;
        t16 = b.*n.*rho.*t3.*2.0;
        t17 = a.*c.*n.*t4.*2.0;
        t18 = a.*b.*c.*n.*rho.*2.0;
        t33 = n.*t3.*t4.*2.0;
        t34 = b.*rho.*t3.*2.0;
        t35 = a.*c.*t4.*t5.*2.0;
        t19 = t12+t13+t14+t15+t16+t17+t18-t33-t34-t35;
        t20 = t9.*t10.*t19;
        t21 = sqrt(t20);
        t22 = a.*n.*rho;
        t23 = 1.0./c;
        t24 = a.*t7;
        t91 = c.*t3;
        t25 = t24-t91;
        t82 = b.*t3;
        t83 = rho.*t3;
        t84 = n.*rho.*t3;
        t85 = n.*rho.*t7;
        t86 = a.*b.*c;
        t87 = a.*c.*rho;
        t88 = a.*c.*n.*rho.*2.0;
        t89 = t82-t83+t84+t85+t86+t87-t88;
        t90 = rho.*t89.*v;
        t92 = a.*rho;
        t93 = c.*n.*rho;
        t94 = a.*c.*t21;
        t95 = -t8-t22+t92+t93+t94;
        t96 = 1.0./t95;
        t97 = t9.*t23.*t25.*v;
        t26 = abs(t97+t2.*t6.*t96.*(t90+rho.*t2.*t23.*t25.*v.*(t8+t22-a.*rho-c.*n.*rho)));
        t27 = a.*(1.0./2.0);
        t28 = c.*(1.0./2.0);
        t29 = t27-t28;
        t30 = t2.*t29;
        t31 = 1.0./n;
        t32 = a.*b.*(1.0./2.0);
        t36 = a.*c.*t21.*(1.0./2.0);
        t104 = a.*rho.*(1.0./2.0);
        t37 = t32+t36-t104;
        t105 = t2.*t6.*t31.*t37;
        t38 = t30-t105;
        t39 = abs(t38);
        t40 = t4.^2;
        t41 = t3.^2;
        t42 = t7.^2;
        t50 = c.*t4.*t40.*t41.*2.0;
        t51 = c.*t11.*t40.*t41.*2.0;
        t52 = a.*n.*t3.*t4.*t7.*t40.*6.0;
        t53 = c.*t4.*t5.*t40.*t41.*6.0;
        t54 = a.*n.*t4.*t5.*t40.*t42.*2.0;
        t55 = c.*n.*t4.*t5.*t40.*t41.*2.0;
        t56 = c.*t3.*t4.*t5.*t7.*t40.*6.0;
        t57 = a.*t3.*t4.*t5.*t7.*t40.*1.2e1;
        t58 = c.*n.*t3.*t4.*t5.*t7.*t40.*6.0;
        t59 = a.*n.*t3.*t4.*t5.*t7.*t40.*6.0;
        t60 = b.*c.*rho.*t40.*t41.*4.0;
        t61 = c.*n.*t4.*t40.*t41.*6.0;
        t62 = rho.*t7.*t21.*t40.*t41.*2.0;
        t63 = b.*c.*n.*rho.*t40.*t41.*8.0;
        t64 = b.*t7.*t21.*t40.*t41.*2.0;
        t65 = a.*c.*n.*rho.*t3.*t7.*t21.*t40.*4.0;
        t66 = n.*rho.*t7.*t21.*t40.*t41.*4.0;
        t67 = c.*n.*t11.*t40.*t41.*2.0;
        t68 = b.*c.*rho.*t5.*t40.*t41.*4.0;
        t69 = rho.*t3.*t5.*t21.*t40.*t42.*2.0;
        t70 = a.*c.*rho.*t3.*t5.*t7.*t21.*t40.*4.0;
        t71 = rho.*t5.*t7.*t21.*t40.*t41.*2.0;
        t72 = a.*n.*t3.*t7.*t11.*t40.*2.0;
        t73 = b.*c.*rho.*t3.*t5.*t7.*t40.*4.0;
        t74 = a.*b.*c.*n.*t3.*t7.*t21.*t40.*2.0;
        t75 = b.*n.*t7.*t21.*t40.*t41.*2.0;
        t76 = t50+t51+t52+t53+t54-t55+t56-t57-t58+t59-t60-t61+t62+t63-t64+t65-t66-t67-t68+t69-t70+t71+t72+t73+t74+t75;
        t77 = sqrt(t76);
        t78 = t3.*t4.*t7.*t21.*v.*2.0;
        t107 = t77-t78;
        t43 = abs(t107);
        t44 = abs(t19);
        t45 = abs(a);
        t46 = abs(c);
        t47 = t45.^2;
        t48 = t46.^2;
        t49 = sqrt(t44);
        t111 = t77+t78;
        t79 = abs(t111);
        t80 = t40.*t44.*t47.*t48.*4.0;
        t81 = t5.*t40.*t44.*t47.*t48.*4.0;
        t98 = t8+t22-t92-t93;
        t99 = rho.*t2.*t23.*t25.*t98.*v;
        t100 = t90+t99;
        t101 = t2.*t6.*t96.*t100;
        t102 = t97+t101;
        t103 = abs(t102);
        t106 = t39.^2;
        t108 = t43.^2;
        t109 = t80+t81+t108;
        t110 = 1.0./sqrt(t109);
        t112 = 1.0./sqrt(t20);
        t113 = t79.^2;
        t114 = t80+t81+t113;
        t115 = 1.0./sqrt(t114);
        t116 = t103.^2;
        t117 = t106+t116+1.0;
        t118 = 1.0./sqrt(t117);
        R = reshape([t38.*1.0./sqrt(t106+t26.^2+1.0),-t102.*t118,t118,t4.*t45.*t46.*t49.*t110.*2.0,-t9.*t10.*t45.*t46.*t49.*t107.*t110.*t112,n.*t4.*t45.*t46.*t49.*t110.*2.0,t4.*t45.*t46.*t49.*t115.*2.0,t9.*t10.*t45.*t46.*t49.*t111.*t112.*t115,n.*t4.*t45.*t46.*t49.*t115.*2.0],[3, 3]);
        
        
        %RINV
        %    RINV = RINV(A,B,C,N,RHO,V)
        
        %    This function was generated by the Symbolic Math Toolbox version 6.1.
        %    12-Jan-2015 13:07:48
        
        t2 = a.^2;
        t3 = rho.^2;
        t4 = n.^2;
        t5 = a.*b;
        t6 = a.*rho;
        t7 = 1.0./a.^2;
        t8 = 1.0./c.^2;
        t9 = b.^2;
        t10 = t2.*t9;
        t11 = t2.*t3;
        t12 = t2.*t3.*t4;
        t13 = c.^2;
        t14 = t3.*t4.*t13;
        t15 = b.*n.*rho.*t2.*2.0;
        t16 = a.*c.*n.*t3.*2.0;
        t17 = a.*b.*c.*n.*rho.*2.0;
        t23 = n.*t2.*t3.*2.0;
        t24 = b.*rho.*t2.*2.0;
        t25 = a.*c.*t3.*t4.*2.0;
        t18 = t10+t11+t12+t14+t15+t16+t17-t23-t24-t25;
        t19 = t7.*t8.*t18;
        t20 = sqrt(t19);
        t21 = a.*c.*t20;
        t22 = c.*n.*rho;
        t27 = a.*n.*rho;
        t39 = t5-t6+t21+t22-t27;
        t26 = abs(t39);
        t30 = -t5+t6+t21+t22-t27;
        t28 = abs(t30);
        t42 = b.*t2;
        t43 = rho.*t2;
        t44 = n.*rho.*t2;
        t45 = n.*rho.*t13;
        t46 = a.*b.*c;
        t47 = a.*c.*rho;
        t48 = a.*t13.*t20;
        t49 = c.*t2.*t20;
        t50 = a.*c.*n.*rho.*2.0;
        t51 = t42-t43+t44+t45+t46+t47+t48-t49-t50;
        t52 = t51.*v;
        t29 = abs(t52);
        t31 = t28.^2;
        t32 = abs(a);
        t33 = t5+t6+t21+t22-t27;
        t34 = 1.0./t33;
        t35 = 1.0./n.^2;
        t36 = 1.0./rho.^2;
        t37 = 1.0./t28.^2;
        t38 = 1.0./t32.^2;
        t40 = t26.^2;
        t41 = t31.*t40;
        t53 = t29.^2;
        t54 = t3.*t4.*t53.*4.0;
        t55 = t32.^2;
        t56 = t3.*t4.*t31.*t55.*4.0;
        t57 = t41+t54+t56;
        t58 = t35.*t36.*t37.*t38.*t57.*(1.0./4.0);
        t59 = sqrt(t58);
        t60 = 1.0./a;
        t61 = 1.0./c;
        t62 = t8.*t9;
        t63 = t3.*t8;
        t64 = t3.*t4.*t7;
        t65 = t3.*t4.*t8;
        t66 = n.*t3.*t60.*t61.*2.0;
        t67 = b.*n.*rho.*t8.*2.0;
        t68 = b.*n.*rho.*t60.*t61.*2.0;
        t72 = n.*t3.*t8.*2.0;
        t73 = b.*rho.*t8.*2.0;
        t74 = t3.*t4.*t60.*t61.*2.0;
        t69 = t62+t63+t64+t65+t66+t67+t68-t72-t73-t74;
        t70 = sqrt(t69);
        t118 = a.*t13.*t70;
        t119 = c.*t2.*t70;
        t120 = t42-t43+t44+t45+t46+t47-t50+t118-t119;
        t121 = t120.*v;
        t71 = abs(t121);
        t75 = a.*c.*t70;
        t116 = t5-t6+t22-t27+t75;
        t76 = abs(t116);
        t77 = t3.^2;
        t78 = t2.^2;
        t79 = t13.^2;
        t85 = c.*t3.*t77.*t78.*2.0;
        t86 = c.*t9.*t77.*t78.*2.0;
        t87 = a.*n.*t2.*t3.*t13.*t77.*6.0;
        t88 = c.*t3.*t4.*t77.*t78.*6.0;
        t89 = a.*n.*t3.*t4.*t77.*t79.*2.0;
        t90 = c.*n.*t3.*t4.*t77.*t78.*2.0;
        t91 = rho.*t13.*t70.*t77.*t78.*2.0;
        t92 = c.*t2.*t3.*t4.*t13.*t77.*6.0;
        t93 = a.*t2.*t3.*t4.*t13.*t77.*1.2e1;
        t94 = c.*n.*t2.*t3.*t4.*t13.*t77.*6.0;
        t95 = a.*n.*t2.*t3.*t4.*t13.*t77.*6.0;
        t96 = b.*c.*rho.*t77.*t78.*4.0;
        t97 = c.*n.*t3.*t77.*t78.*6.0;
        t98 = rho.*t2.*t4.*t70.*t77.*t79.*2.0;
        t99 = a.*c.*rho.*t2.*t4.*t13.*t70.*t77.*4.0;
        t100 = rho.*t4.*t13.*t70.*t77.*t78.*2.0;
        t101 = b.*c.*n.*rho.*t77.*t78.*8.0;
        t102 = c.*n.*t9.*t77.*t78.*2.0;
        t103 = b.*c.*rho.*t4.*t77.*t78.*4.0;
        t104 = b.*t13.*t70.*t77.*t78.*2.0;
        t105 = a.*c.*n.*rho.*t2.*t13.*t70.*t77.*4.0;
        t106 = n.*rho.*t13.*t70.*t77.*t78.*4.0;
        t107 = a.*n.*t2.*t9.*t13.*t77.*2.0;
        t108 = b.*c.*rho.*t2.*t4.*t13.*t77.*4.0;
        t109 = a.*b.*c.*n.*t2.*t13.*t70.*t77.*2.0;
        t110 = b.*n.*t13.*t70.*t77.*t78.*2.0;
        t111 = t85+t86+t87+t88+t89-t90+t91+t92-t93-t94+t95-t96-t97+t98-t99+t100+t101-t102-t103-t104+t105-t106+t107+t108+t109+t110;
        t112 = sqrt(t111);
        t141 = t2.*t3.*t13.*t70.*v.*2.0;
        t142 = t112-t141;
        t80 = abs(t142);
        t81 = abs(c);
        t82 = abs(t18);
        t83 = t81.^2;
        t84 = -t5+t6+t22-t27+t75;
        t113 = sqrt(t82);
        t114 = abs(t84);
        t115 = 1.0./t114.^2;
        t117 = t76.^2;
        t122 = t71.^2;
        t123 = t114.^2;
        t127 = rho.*t13.*t20.*t77.*t78.*2.0;
        t128 = b.*t13.*t20.*t77.*t78.*2.0;
        t129 = a.*c.*n.*rho.*t2.*t13.*t20.*t77.*4.0;
        t130 = n.*rho.*t13.*t20.*t77.*t78.*4.0;
        t131 = rho.*t2.*t4.*t20.*t77.*t79.*2.0;
        t132 = a.*c.*rho.*t2.*t4.*t13.*t20.*t77.*4.0;
        t133 = rho.*t4.*t13.*t20.*t77.*t78.*2.0;
        t134 = a.*b.*c.*n.*t2.*t13.*t20.*t77.*2.0;
        t135 = b.*n.*t13.*t20.*t77.*t78.*2.0;
        t136 = t85+t86+t87+t88+t89-t90+t92-t93-t94+t95-t96-t97+t101-t102-t103+t107+t108+t127-t128+t129-t130+t131-t132+t133+t134+t135;
        t187 = sqrt(t136);
        t188 = t2.*t3.*t13.*t20.*v.*2.0;
        t124 = abs(t187-t188);
        t125 = t55.*t77.*t82.*t83.*4.0;
        t126 = t4.*t55.*t77.*t82.*t83.*4.0;
        t137 = t38.*t115.*t122;
        t138 = t35.*t36.*t38.*t117.*(1.0./4.0);
        t139 = t137+t138+1.0;
        t140 = 1.0./sqrt(t139);
        t143 = t80.^2;
        t144 = t125+t126+t143;
        t145 = sqrt(t144);
        t146 = 1.0./t84;
        t147 = a.*t3.*t32.*t81.*t112.*t113;
        t148 = a.*b.*rho.*t32.*t81.*t112.*t113;
        t149 = c.*n.*t3.*t32.*t81.*t112.*t113;
        t150 = a.*c.*rho.*t32.*t70.*t81.*t112.*t113;
        t160 = a.*n.*t3.*t32.*t81.*t112.*t113;
        t151 = t147+t148+t149+t150-t160;
        t152 = 1.0./t151;
        t153 = t117.*t123;
        t154 = t3.*t4.*t122.*4.0;
        t155 = t3.*t4.*t55.*t123.*4.0;
        t156 = t153+t154+t155;
        t157 = t35.*t36.*t38.*t115.*t156.*(1.0./4.0);
        t158 = sqrt(t157);
        t192 = t112+t141;
        t159 = abs(t192);
        t161 = a.*rho.*t112;
        t162 = n.*t4.*t77.*t79.*v.*2.0;
        t163 = a.*c.*t2.*t77.*v.*2.0;
        t164 = c.*n.*rho.*t112;
        t165 = a.*c.*t70.*t112;
        t166 = a.*c.*t2.*t3.*t9.*v.*2.0;
        t167 = n.*t2.*t13.*t77.*v.*6.0;
        t168 = a.*c.*t4.*t13.*t77.*v.*6.0;
        t169 = a.*c.*t2.*t4.*t77.*v.*6.0;
        t170 = a.*rho.*t2.*t3.*t13.*t70.*v.*2.0;
        t171 = n.*t2.*t4.*t13.*t77.*v.*6.0;
        t172 = c.*n.*rho.*t2.*t3.*t13.*t70.*v.*4.0;
        t173 = a.*rho.*t3.*t4.*t70.*t79.*v.*2.0;
        t174 = n.*t2.*t3.*t9.*t13.*v.*2.0;
        t175 = a.*rho.*t2.*t3.*t4.*t13.*t70.*v.*2.0;
        t176 = a.*b.*c.*n.*rho.*t2.*t3.*v.*8.0;
        t177 = a.*b.*c.*rho.*t3.*t4.*t13.*v.*4.0;
        t178 = b.*c.*n.*t2.*t3.*t13.*t70.*v.*2.0;
        t179 = a.*b.*n.*t2.*t3.*t13.*t70.*v.*2.0;
        t180 = 1.0./sqrt(t82);
        t181 = 1.0./t32;
        t182 = 1.0./t81;
        t183 = t37.*t38.*t53;
        t184 = t35.*t36.*t38.*t40.*(1.0./4.0);
        t185 = t183+t184+1.0;
        t186 = 1.0./sqrt(t185);
        t189 = abs(t187+t188);
        t190 = 1.0./sqrt(t136);
        t191 = 1.0./n;
        t193 = t159.^2;
        t194 = t125+t126+t193;
        t195 = sqrt(t194);
        t196 = b.*n.*t2.*t112;
        t197 = rho.*t2.*t4.*t112;
        t198 = rho.*t4.*t13.*t112;
        t199 = a.*b.*c.*n.*t112;
        t200 = a.*c.*n.*rho.*t112;
        t201 = t69.^(3.0./2.0);
        t202 = rho.*t78.*t79.*t201.*v;
        t203 = a.*n.*t13.*t70.*t112;
        t204 = b.*t3.*t13.*t70.*t78.*v.*2.0;
        t205 = n.*rho.*t3.*t13.*t70.*t78.*v.*2.0;
        t206 = a.*c.*rho.*t2.*t3.*t4.*t13.*t70.*v.*2.0;
        Rinv = reshape([a.*n.*rho.*t34.*t59.*-2.0,a.*t140.*t145.*t146.*t152.*t158.*(t161+t162+t163+t164+t165+t166+t167+t168+t169+t170+t171+t172+t173+t174+t175+t176+t177+t178+t179-a.*b.*t112-a.*n.*rho.*t112-t2.*t4.*t13.*t77.*v.*1.2e1-a.*c.*n.*t2.*t77.*v.*6.0-a.*b.*c.*rho.*t2.*t3.*v.*4.0-a.*c.*n.*t2.*t3.*t9.*v.*2.0-a.*c.*n.*t2.*t4.*t77.*v.*2.0-a.*c.*n.*t4.*t13.*t77.*v.*6.0-a.*b.*t2.*t3.*t13.*t70.*v.*2.0-a.*b.*c.*rho.*t2.*t3.*t4.*v.*4.0-a.*n.*rho.*t2.*t3.*t13.*t70.*v.*4.0-c.*rho.*t2.*t3.*t4.*t13.*t70.*v.*4.0).*(1.0./2.0),a.*t140.*t146.*t152.*t158.*t195.*(-t161+t162+t163-t164-t165+t166+t167+t168+t169+t170+t171+t172+t173+t174+t175+t176+t177+t178+t179+a.*b.*t112+a.*n.*rho.*t112-t2.*t4.*t13.*t77.*v.*1.2e1-a.*c.*n.*t2.*t77.*v.*6.0-a.*b.*c.*rho.*t2.*t3.*v.*4.0-a.*c.*n.*t2.*t3.*t9.*v.*2.0-a.*c.*n.*t2.*t4.*t77.*v.*2.0-a.*c.*n.*t4.*t13.*t77.*v.*6.0-a.*b.*t2.*t3.*t13.*t70.*v.*2.0-a.*b.*c.*rho.*t2.*t3.*t4.*v.*4.0-a.*n.*rho.*t2.*t3.*t13.*t70.*v.*4.0-c.*rho.*t2.*t3.*t4.*t13.*t70.*v.*4.0).*(-1.0./2.0),0.0,t2.*t13.*t20.*t59.*t180.*t181.*t182.*t186.*t190.*sqrt(t125+t126+t124.^2).*(-1.0./2.0),t2.*t13.*t20.*t59.*t180.*t181.*t182.*t186.*t190.*sqrt(t125+t126+t189.^2).*(1.0./2.0),a.*rho.*t34.*t59.*2.0,t140.*t145.*t146.*t152.*t158.*t191.*(t196+t197+t198+t199+t200+t202+t203+t204+t205+t206-n.*rho.*t2.*t112-a.*c.*rho.*t4.*t112.*2.0-c.*n.*t2.*t70.*t112-rho.*t3.*t13.*t70.*t78.*v-rho.*t9.*t13.*t70.*t78.*v-b.*n.*t3.*t13.*t70.*t78.*v.*2.0-rho.*t2.*t3.*t4.*t70.*t79.*v-rho.*t3.*t4.*t13.*t70.*t78.*v-a.*b.*c.*n.*t2.*t3.*t13.*t70.*v.*2.0-a.*c.*n.*rho.*t2.*t3.*t13.*t70.*v.*2.0).*(1.0./2.0),t140.*t146.*t152.*t158.*t191.*t195.*(t196+t197+t198+t199+t200-t202+t203-t204-t205-t206-n.*rho.*t2.*t112-a.*c.*rho.*t4.*t112.*2.0-c.*n.*t2.*t70.*t112+rho.*t3.*t13.*t70.*t78.*v+rho.*t9.*t13.*t70.*t78.*v+b.*n.*t3.*t13.*t70.*t78.*v.*2.0+rho.*t2.*t3.*t4.*t70.*t79.*v+rho.*t3.*t4.*t13.*t70.*t78.*v+a.*b.*c.*n.*t2.*t3.*t13.*t70.*v.*2.0+a.*c.*n.*rho.*t2.*t3.*t13.*t70.*v.*2.0).*(1.0./2.0)],[3, 3]);
        
        
        %LAMBDA
        %    LAMBDA = LAMBDA(A,B,C,N,RHO,V)
        
        %    This function was generated by the Symbolic Math Toolbox version 6.1.
        %    12-Jan-2015 13:06:43
        
        t2 = 1.0./c.^2;
        t3 = rho.^2;
        t4 = 1.0./a.^2;
        t5 = n.^2;
        t6 = 1.0./a;
        t7 = 1.0./c;
        t8 = a.^2;
        t9 = t3.^2;
        t10 = t8.^2;
        t11 = b.^2;
        t12 = c.^2;
        t13 = t2.*t11;
        t14 = t2.*t3;
        t15 = t3.*t4.*t5;
        t16 = t2.*t3.*t5;
        t17 = n.*t3.*t6.*t7.*2.0;
        t18 = b.*n.*rho.*t2.*2.0;
        t19 = b.*n.*rho.*t6.*t7.*2.0;
        t22 = n.*t2.*t3.*2.0;
        t23 = b.*rho.*t2.*2.0;
        t24 = t3.*t5.*t6.*t7.*2.0;
        t20 = t13+t14+t15+t16+t17+t18+t19-t22-t23-t24;
        t21 = t12.^2;
        t25 = sqrt(t20);
        t26 = sqrt(2.0);
        t27 = 1.0./rho.^2;
        t28 = 1.0./sqrt(t20);
        t29 = c.*t3.*t9.*t10;
        t30 = c.*t9.*t10.*t11;
        t31 = a.*n.*t3.*t8.*t9.*t12.*3.0;
        t32 = c.*t3.*t5.*t9.*t10.*3.0;
        t33 = a.*n.*t3.*t5.*t9.*t21;
        t34 = rho.*t9.*t10.*t12.*t25;
        t35 = c.*t3.*t5.*t8.*t9.*t12.*3.0;
        t36 = a.*n.*t3.*t5.*t8.*t9.*t12.*3.0;
        t37 = rho.*t5.*t8.*t9.*t21.*t25;
        t38 = rho.*t5.*t9.*t10.*t12.*t25;
        t39 = b.*c.*n.*rho.*t9.*t10.*4.0;
        t40 = a.*c.*n.*rho.*t8.*t9.*t12.*t25.*2.0;
        t41 = a.*n.*t8.*t9.*t11.*t12;
        t42 = b.*c.*rho.*t5.*t8.*t9.*t12.*2.0;
        t43 = a.*b.*c.*n.*t8.*t9.*t12.*t25;
        t44 = b.*n.*t9.*t10.*t12.*t25;
        t45 = t29+t30+t31+t32+t33+t34+t35+t36+t37+t38+t39+t40+t41+t42+t43+t44-b.*c.*rho.*t9.*t10.*2.0-c.*n.*t3.*t9.*t10.*3.0-c.*n.*t9.*t10.*t11-b.*t9.*t10.*t12.*t25-c.*n.*t3.*t5.*t9.*t10-a.*t3.*t5.*t8.*t9.*t12.*6.0-n.*rho.*t9.*t10.*t12.*t25.*2.0-b.*c.*rho.*t5.*t9.*t10.*2.0-c.*n.*t3.*t5.*t8.*t9.*t12.*3.0-a.*c.*rho.*t5.*t8.*t9.*t12.*t25.*2.0;
        t46 = sqrt(t45);
        Lambda = reshape([v,0.0,0.0,0.0,v-t2.*t4.*t26.*t27.*t28.*t46.*(1.0./2.0),0.0,0.0,0.0,v+t2.*t4.*t26.*t27.*t28.*t46.*(1.0./2.0)],[3, 3]);
        
        
        
        
        if(strcmp(sign,'minus'))
            Lambda_sign = 1/2*(Lambda-abs(Lambda));
        else
            Lambda_sign = 1/2*(Lambda+abs(Lambda));
        end
        
end

function [R,Rinv,Lambda_sign] = rotationMatrix_J_n0(rho,v,M,sign)
        
        % Similar transformation R, and inv(R): Note that A=R*Lambda*RI
        
        k0 = M.k0;
        R = M.R; T = M.T;
        rho0 = M.rho_l0; p0 = M.p0;
        
        a = 1./(R.*T);
        b = rho0.*(1-p0./k0);
        c = rho0./k0;
        
        %R
        %    R = R(A,B,C,RHO,V)
        
        %    This function was generated by the Symbolic Math Toolbox version 6.3.
        %    05-Nov-2015 00:41:58
        
        t2 = b-rho;
        t3 = 1.0./c.^2;
        t4 = t2.^2;
        t5 = t3.*t4;
        t6 = sqrt(t5);
        t7 = c.^2;
        t8 = c.*t6;
        t9 = b.^2;
        t10 = t3.*t9;
        t11 = rho.^2;
        t12 = t3.*t11;
        t13 = t10+t12-b.*rho.*t3.*2.0;
        t14 = sqrt(t13);
        t15 = c.*t14;
        t16 = -b+rho+t15;
        t17 = sqrt(c.*t2.*t16.*-2.0);
        t18 = t6.*t7.*v.*2.0;
        t19 = v.^2;
        t20 = t6.*t7.*t19.*2.0;
        t21 = b-rho-t8+t20;
        t22 = 1.0./t21;
        t23 = 1.0./a;
        t24 = b.*c;
        t25 = c.*rho;
        t26 = t6.*t7;
        t27 = t24+t25+t26;
        t28 = -b+rho+t8;
        t29 = 1.0./t28;
        R = reshape([-t23.*t27.*t29+1.0,v-t23.*t27.*t29.*v,1.0,t22.*(t17+t18),1.0,0.0,-t22.*(t17-t18),1.0,0.0],[3,3]);
        
        
        %RINV
        %    RINV = RINV(A,B,C,RHO,V)
        
        %    This function was generated by the Symbolic Math Toolbox version 6.3.
        %    05-Nov-2015 00:42:48
        
        t2 = b-rho;
        t3 = 1.0./c.^2;
        t4 = t2.^2;
        t5 = t3.*t4;
        t6 = sqrt(t5);
        t7 = c.^2;
        t8 = b.^2;
        t9 = rho.^2;
        t17 = b.*rho.*2.0;
        t10 = t8+t9-t17;
        t11 = t3.*t10;
        t12 = sqrt(t11);
        t13 = c.*t12;
        t14 = -b+rho+t13;
        t18 = c.*t2.*t14.*2.0;
        t15 = 1.0./sqrt(-t18);
        t16 = c.*t6;
        t19 = v.^2;
        t20 = t6.*t7.*t19.*2.0;
        t21 = 1.0./a;
        t22 = -b+rho+t16;
        t23 = 1.0./t22;
        t24 = sqrt(-t18);
        t25 = t24.*v;
        t26 = a.*b;
        t27 = b.*c;
        t28 = c.*rho;
        t29 = t6.*t7;
        t30 = t26+t27+t28+t29-a.*rho-a.*c.*t6;
        Rinv = reshape([0.0,t15.*(b-rho+t20-c.*t6).*(1.0./2.0),t15.*(b-rho-t16+t20).*(-1.0./2.0),0.0,-t6.*t7.*t15.*v+1.0./2.0,t6.*t7.*t15.*v+1.0./2.0,1.0,t15.*t21.*t23.*t30.*(b-rho-t16+t25).*(1.0./2.0),t15.*t21.*t23.*t30.*(-b+rho+t16+t25).*(1.0./2.0)],[3,3]);
        
        
        %LAMBDA
        %    LAMBDA = LAMBDA(B,C,RHO,V)
        
        %    This function was generated by the Symbolic Math Toolbox version 6.3.
        %    05-Nov-2015 00:55:49
        
        t2 = 1.0./c.^2;
        t3 = b-rho;
        t4 = t3.^2;
        t5 = t2.*t4;
        t6 = 1.0./sqrt(t5);
        t7 = b.^2;
        t8 = rho.^2;
        t9 = t7+t8-b.*rho.*2.0;
        t10 = t2.*t9;
        t11 = sqrt(t10);
        t12 = c.*t11;
        t13 = -b+rho+t12;
        t14 = sqrt(c.*t3.*t13.*-2.0);
        Lambda = reshape([v,0.0,0.0,0.0,v-t2.*t6.*t14.*(1.0./2.0),0.0,0.0,0.0,v+t2.*t6.*t14.*(1.0./2.0)],[3,3]);
        
        if(strcmp(sign,'minus'))
            Lambda_sign = 1/2*(Lambda-abs(Lambda));
        else
            Lambda_sign = 1/2*(Lambda+abs(Lambda));
        end
        
    end


end