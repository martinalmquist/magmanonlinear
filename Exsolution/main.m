% Main code for 1D nonlinear magma wave equation
% N = number of grid points
% order = internal order of accuracy
% plot_solution = true to plot solution at all time steps
%
% FORM ('cons' or 'prim' or 'prim_rho_v_n')
% 'cons' - conservative formulation, variables q = [rho, rho*v, rho*n]';
% 'prim' - primitive (quasilinear) formulation, variables q = [rho, rho*v, rho*n]';
% 'prim_rho_v_n' - primitive (quasilinear) formulation, variables q = [rho, v, n]';
%
% UPWIND (true or false)
% For the two primitive formulations, artificial dissipation can be
% introduced via 'upwinding', i.e B*D*q is replaced by B_+*D_+*q + B_-*D_-*q.
% The conservative formulation does not have the upwinding option.
%
% AD (true or false)
% Artificial dissipation of the form
% q1_t = ... + Hinv*D_p^T*C1*D_p*q1;
% q2_t = ... + Hinv*D_p^T*C2*D_p*q2;
% q3_t = ... + Hinv*D_p^T*C3*D_p*q3;
%
% RUSSIAN (true or false)
% Ken's new Russian operators with built-in artificial dissipation.
%
% out = data structure containing solution, parameters, etc.
%

function out=main(N,order,plot_solution,PROBLEM,UPWIND,AD,FORM,RUSSIAN)

% Add subfolders to MATLAB path.
addSubpaths;
% close all;

% Default options
if (nargin <= 7 || isempty(RUSSIAN)); RUSSIAN = true; end;
if (nargin <= 6 || isempty(FORM)); FORM = 'cons'; end;
if (nargin <= 5 || isempty(AD)); AD = false; end;
if (nargin <= 4 || isempty(UPWIND)); UPWIND = false; end;
if (nargin <= 3 || isempty(PROBLEM)); PROBLEM = @pressureDropSurface; end;
if (nargin <= 2 || isempty(plot_solution)); plot_solution = true; end;
if (nargin == 1 || isempty(order)); order = 8; end;

% total simulation time
tmax = 100;

available_forms = {'cons','prim','prim_rho_v_n'};
if(strcmp(FORM,'prim_rho_v_n')); error('That form is no longer available'); end;
if(~strcmp(FORM,available_forms)); error('That form is not available'); end;

%%% Artificial damping parameters, tuned with tau = 0.1 %%%
[C_AD,C_AD_boundary,C_upwind,C_upwind_boundary]...
    = dissipationParameters(order,FORM,AD);

%%% CFL constant for time-stepping %%
CFL = CFLconstant(order);


% Problem parameters stored in struct
M = problemParameters();
L = M.L; % Length of conduit [m]
if(order == 8 && ~ RUSSIAN)
    % Special distribution of grid points for the 8th order operator
    [x,dx] = gridPointsSBP8(N,L);
else
    dx = L/N; % length of domain, grid spacing
    x = (0:dx:L)'; % coordinates (at N+1 grid points)
end
M.x = x;
M.N = N; M.CFL = CFL; M.dx = dx;

% operator splitting to handle stiffness from exsolution
% TO DO: Implement IMEX Runge-Kutta.
M.operator_splitting = false; % true or false

% SBP differentiation matrix
% D = first derivative
% Hinv = corner entry of diagonal norm (for SAT penalties)
% ADO = artificial dissipation operator
% order = interior order of accuracy
OPERATORS = struct;
if(RUSSIAN)
    [D,Hinv,~,DISS,~,~,Hinvfull] = SBPoperatorsRussian(N,dx,order);
    OPERATORS.DISS = DISS;
else
    [Dplus,Dminus,ADO,ADO_boundary] = SBPoperators_upwind(N,dx,order,C_upwind,C_upwind_boundary);
    [D,Hinv,H] = SBPoperators(N,dx,order);
    OPERATORS.Dplus = Dplus; OPERATORS.Dminus = Dminus;
    OPERATORS.ADO = ADO; OPERATORS.ADO_boundary = ADO_boundary;
    OPERATORS.C_AD = C_AD; OPERATORS.C_AD_boundary = C_AD_boundary;
    Hinvfull = inv(H);
end
OPERATORS.D = D; OPERATORS.Hinv = Hinv; OPERATORS.Hinvfull = Hinvfull;
OPERATORS.D2 = dissipationOperators(N,2);
OPERATORS.D3 = dissipationOperators(N,3);


% Runge-Kutta coefficients (4th order, low storage method)
RK = RKLS4();

% Conservative variables
% q1 = rho
% q2 = rho*v
% q3 = rho*n

% rates arrays
Dq1 = zeros(N+1,1); % dq1/dt
Dq2 = zeros(N+1,1); % dq2/dt
Dq3 = zeros(N+1,1); % dq3/dt

%%% Initial data and boundary conditions. %%%
M = PROBLEM(M);
% Solve ODE for equilibrium initial condition
[rho1,v1,n1,p1] = icSolver(M);
M = PROBLEM(M,p1,rho1,v1,n1);
% Solve PDE to steady state to get "numerical" initial condition
% [rho0,v0,n0,p0] = steadyStateSolver(M,rho1,v1,n1,p1,OPERATORS,UPWIND,FORM,RUSSIAN);
rho0 = rho1;
v0 = v1;
n0 = n1;
p0 = p1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Steady state reached. Restarting.');
fh = figure();
plotSolution(fh, x,0,M,rho0,rho0.*v0,rho0.*n0,FORM);
title('Steady state for SBP-SAT');
fh = figure();
plotSolution(fh, x,0,M,rho1,rho1.*v1,rho1.*n1,FORM);
title('Steady state for ODE45');
fh = figure();
plotSolution(fh, x,0,M,rho0-rho1,(rho0-rho1).*(v0-v1),(rho0-rho1).*(n0-n1),FORM,false);
title('Difference');

% fields arrays
if(strcmp(FORM,'prim_rho_v_n'))
    q1 = rho0; q2 = v0; q3 = n0;
else
    q1=rho0; q2=rho0.*v0; q3=rho0.*n0;
end

%%% Time step %%%
lmax = maxEig(rho0,v0,n0,M);
dt = CFL*dx/lmax;
nt = ceil(tmax/dt); % number of time steps (approximately, time step is adapted)
% dt = tmax/nt; % adjust dt to finish exactly at t=tmax

% storage arrays for all time steps
% (There may be more/fewer time steps since the time step is adapted.)
out.rho = zeros(N+1,nt+1); out.v = zeros(N+1,nt+1);
out.n = zeros(N+1,nt+1); out.p = zeros(N+1,nt+1);
out.t = zeros(1,nt+1);

% save initial conditions
out.rho(:,1) = rho0; out.v(:,1) = v0; out.n(:,1) = n0; out.p(:,1) = p0;

% save space and time vectors
out.t = (0:nt)*dt; out.x = x;

% loop over time steps
t = 0; % start at t=0
nt = 0; % number of time steps taken

% Setup plot
fh = figure;
line_handles = plotSolution(fh,x,t,M,q1,q2,q3,FORM);
% pause;

tic
while t < tmax

  [t,q1,q2,q3,Dq1,Dq2,Dq3] = RKLSstep(RK,q1,q2,q3,t,dt,Dq1,Dq2,Dq3,M,OPERATORS,UPWIND,FORM,RUSSIAN);
  nt = nt+1;
  out.t(nt+1) = t;

  % save fields and pressure
  if( strcmp(FORM,'cons') || strcmp(FORM,'prim') )
    rho = q1; v = q2./q1; n = q3./q1;
  else
    rho = q1; v = q2; n = q3;
  end
  p = p_func(rho,n,M);
  out.rho(:,nt+1) = rho;
  out.v(:,nt+1) = v;
  out.n(:,nt+1) = n;
  out.p(:,nt+1) = p;

  % Adaptive time step
  lmax = maxEig(rho,v,n,M);
  dt = CFL*dx/lmax;
  if(t+dt>tmax); dt = tmax - t; end;

  %%% PLOTTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if plot_solution
    if mod(nt,20)==0
        c = sound_speed(rho,n,M);
        updatePlot(fh,line_handles,rho,v,n,p,c,t,M);
    end
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end
toc

% return other variables as well
out.M = M;
out.L = L;
out.N = N;
out.dt = dt;
out.nt = nt;