function M = problemParameters()

% material properties (stored in data structure M)
%
% M.g = gravitational acceleration
% M.tau = gas exsolution time scale

M.g = 9.8; % acceleration due to gravity [m/s^2]

M.tau = 1; % exsolution time scale [s]
M.L = 8000; % Length of conduit [m]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%% For n_eq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M.s = 4*1e-6; % solubility constant [sqrt(Pascal)]
M.n0 = 0.04; % total volatile content []
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%% PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%
M.R = 461; % specific gas constant [J/kgK]
M.T = 900; % temperature [K]
M.rho_l0 = 2600; % liquid density [kg/m^3]
M.p0 = M.n0^2/M.s^2; % exsolution pressure [Pa]
M.k0 = 10e9; % bulk modulus of liquid [Pa]
M.pa = 101325; % Atmospheric pressure [Pa]
M.mu = 1e6; % viscosity [Pa s]
M.f0 = 0.01; % Darcy-Weisbach friction factor []
M.phi0 = 0.75; % critical gas volume fraction for fragmentation
M.radius = 30; % conduit radius [m]

% Damping parameters, only used to solve to steady state.
M.alpha0 = 0; 
M.alpha1 = 0; 
M.alpha2 = 0; 
M.alpha3 = 0;

