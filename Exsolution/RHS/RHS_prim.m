function [Dq1, Dq2, Dq3]=RHS_prim(UPWIND,t,q1,q2,q3,M,OPERATORS,RUSSIAN)
% RHS in PDE, primitive form
    
D = OPERATORS.D; Hinv = OPERATORS.Hinv;

% Compute needed quantities
rho = q1; v = q2./q1; n = q3./q1;
p = p_func(rho,n,M);
n_eq = n_eq_func(p,M);
mdot = mdot_func(rho,n,n_eq,M);

if(UPWIND)
    Dplus = OPERATORS.Dplus; Dminus = OPERATORS.Dminus;
    
    % Build matrices Bplus and Bminus (upwind and downwind flux Jacobians)
    [Bp11, Bp12, Bp13,...
     Bp21, Bp22, Bp23,...
     Bp31, Bp32, Bp33,...
     Bm11, Bm12, Bm13,...
     Bm21, Bm22, Bm23,...
     Bm31, Bm32, Bm33] = build_Jp_Jm(rho,v,n,M);
 
    % Differentiate
    q1_x_plus = Dplus*q1; q2_x_plus = Dplus*q2; q3_x_plus = Dplus*q3;
    q1_x_minus = Dminus*q1; q2_x_minus = Dminus*q2; q3_x_minus = Dminus*q3;
        
    % Derivative terms
    Dq1_plus = -Bp11.*q1_x_plus - Bp12.*q2_x_plus - Bp13.*q3_x_plus;
    Dq2_plus = -Bp21.*q1_x_plus - Bp22.*q2_x_plus - Bp23.*q3_x_plus;
    Dq3_plus = -Bp31.*q1_x_plus - Bp32.*q2_x_plus - Bp33.*q3_x_plus;
    
    Dq1_minus = -Bm11.*q1_x_minus - Bm12.*q2_x_minus - Bm13.*q3_x_minus;
    Dq2_minus = -Bm21.*q1_x_minus - Bm22.*q2_x_minus - Bm23.*q3_x_minus;
    Dq3_minus = -Bm31.*q1_x_minus - Bm32.*q2_x_minus - Bm33.*q3_x_minus;
    
    Dq1 = Dq1_plus + Dq1_minus;
    Dq2 = Dq2_plus + Dq2_minus;
    Dq3 = Dq3_plus + Dq3_minus;
    
else
    % Build matrix B (Flux Jacobian)
    [B11, B12, B13,...
     B21, B22, B23,...
     B31, B32, B33] = build_J_matrix(rho,v,n,M);

    % Differentiate
    q1_x = D*q1; q2_x = D*q2; q3_x = D*q3;

    % Derivative terms
    Dq1 = -B11.*q1_x - B12.*q2_x - B13.*q3_x;
    Dq2 = -B21.*q1_x - B22.*q2_x - B23.*q3_x;
    Dq3 = -B31.*q1_x - B32.*q2_x - B33.*q3_x;
end

if(RUSSIAN)
    % Russian dissipation!
    DISS = OPERATORS.DISS;
    alpha = maxEig(rho,v,n,M);
    Dq1 = Dq1 + alpha*DISS*q1;
    Dq2 = Dq2 + alpha*DISS*q2;
    Dq3 = Dq3 + alpha*DISS*q3;
else
    C_AD = OPERATORS.C_AD;
    C_AD_boundary = OPERATORS.C_AD_boundary;
    ADO = OPERATORS.ADO;
    ADO_boundary = OPERATORS.ADO_boundary;
    %%%%%%%%%%%%%%%% Ordinary AD %%%%%%%%%%%%%%%%%%%%%%%%%%
    Dq1 = Dq1 - (C_AD(1)*ADO + C_AD_boundary(1)*ADO_boundary)*q1;
    Dq2 = Dq2 - (C_AD(2)*ADO + C_AD_boundary(2)*ADO_boundary)*q2;
    Dq3 = Dq3 - (C_AD(3)*ADO + C_AD_boundary(3)*ADO_boundary)*q3;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

% Lower order terms in conservative formulation
[G1, G2, G3] = G_func(mdot,rho,v,M,OPERATORS);

% Add lower order terms
Dq1 = Dq1 + G1;
Dq2 = Dq2 + G2;
Dq3 = Dq3 + G3;

%%%%%%% SAT terms %%%%%%%%%%%%%%%%%

%======= Bottom boundary =================%
SAT = BC_bottom_cons(t,rho,v,n,q1,q2,q3,M,Hinv);

% Add SAT contributions
Dq1(1) = Dq1(1) + SAT(1);
Dq2(1) = Dq2(1) + SAT(2);
Dq3(1) = Dq3(1) + SAT(3);
%=========================================%

%======= Top boundary =================%
SAT = BC_top_cons(t,rho,v,n,q1,q2,q3,M,Hinv);

% Add SAT contributions
Dq1(end) = Dq1(end) + SAT(1);
Dq2(end) = Dq2(end) + SAT(2);
Dq3(end) = Dq3(end) + SAT(3);
%=========================================%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

function mdot = mdot_func(rho,n,n_eq,M)
    mdot = -rho.*(n-n_eq)./M.tau;
end

function [G1, G2, G3] = G_func(mdot,rho,v,M,OPERATORS)
    % Lower order terms in conservative formulation
    G1 = 0;
    G2 = -rho*M.g; % + p*dA/dx + drag;
    G3 = mdot; 
    
    % Damping when driving towards steady state.
    D1 = OPERATORS.D; Hinv = OPERATORS.Hinvfull;
    D2 = OPERATORS.D2;
    D3 = OPERATORS.D3;
    G2 = G2 - M.alpha0*v; 
    G2 = G2 - M.alpha1*Hinv*M.dx^2*(D1'*D1)*v;
    G2 = G2 - M.alpha2*Hinv*(D2'*D2)*v;
    G2 = G2 - M.alpha3*Hinv*(D3'*D3)*v;
end
