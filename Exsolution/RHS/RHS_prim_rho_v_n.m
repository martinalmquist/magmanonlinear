function [Drho, Dv, Dn]=RHS_prim_rho_v_n(...
    UPWIND,t,rho,v,n,M,OPERATORS)
% RHS in PDE, primitive form, variables rho, v, n.

% Compute needed quantities
p = p_func(rho,n,M);
n_eq = n_eq_func(p,M);
mdot = mdot_func(rho,n,n_eq,M);

Dplus = OPERATORS.Dplus; Dminus = OPERATORS.Dminus;
D = OPERATORS.D; Hinv = OPERATORS.Hinv;
ADO = OPERATORS.ADO; ADO_boundary = OPERATORS.ADO_boundary;
C_AD = OPERATORS.C_AD; C_AD_boundary = OPERATORS.C_AD_boundary;

if(UPWIND)
    % Build matrices Bplus and Bminus
    [Bp11, Bp12, Bp13,...
     Bp21, Bp22, Bp23,...
     Bp31, Bp32, Bp33,...
     Bm11, Bm12, Bm13,...
     Bm21, Bm22, Bm23,...
     Bm31, Bm32, Bm33] = build_Bp_Bm(rho,v,n,M);

    % Differentiate
    rho_x_plus = Dplus*rho; rho_x_minus = Dminus*rho;
    v_x_plus = Dplus*v; v_x_minus = Dminus*v;
    n_x_plus = Dplus*n; n_x_minus = Dminus*n;

    % Derivative terms
    Drho_plus = -Bp11.*rho_x_plus - Bp12.*v_x_plus - Bp13.*n_x_plus;
    Dv_plus = -Bp21.*rho_x_plus - Bp22.*v_x_plus - Bp23.*n_x_plus;
    Dn_plus = -Bp31.*rho_x_plus - Bp32.*v_x_plus - Bp33.*n_x_plus;

    Drho_minus = -Bm11.*rho_x_minus - Bm12.*v_x_minus - Bm13.*n_x_minus;
    Dv_minus = -Bm21.*rho_x_minus - Bm22.*v_x_minus - Bm23.*n_x_minus;
    Dn_minus = -Bm31.*rho_x_minus - Bm32.*v_x_minus - Bm33.*n_x_minus;

    Drho = Drho_plus + Drho_minus;
    Dv = Dv_plus + Dv_minus;
    Dn = Dn_plus + Dn_minus;
else
    % Build matrix B
    [B11, B12, B13,...
     B21, B22, B23,...
     B31, B32, B33] = build_B_matrix(rho,v,n,M);

    % Differentiate
    rho_x = D*rho; v_x = D*v; n_x = D*n;

    % Derivative terms
    Drho = -B11.*rho_x - B12.*v_x - B13.*n_x;
    Dv = -B21.*rho_x - B22.*v_x - B23.*n_x;
    Dn = -B31.*rho_x - B32.*v_x - B33.*n_x;
end

%%%%%%%%%%%%%%%% Ordinary AD %%%%%%%%%%%%%%%%%%%%%%%%%%
Drho = Drho - (C_AD(1)*ADO + C_AD_boundary(1)*ADO_boundary)*rho;
Dv = Dv - (C_AD(2)*ADO + C_AD_boundary(2)*ADO_boundary)*v;
Dn = Dn - (C_AD(3)*ADO + C_AD_boundary(3)*ADO_boundary)*n;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Lower order terms in conservative formulation
[G1, G2, G3] = G_func(mdot,rho,M);

% Jacobian inverse
[J_UI11, J_UI12, J_UI13,...
 J_UI21, J_UI22, J_UI23,...
 J_UI31, J_UI32, J_UI33] = build_Jacobian_inverse(rho,v,n);

% Multiply lower order terms by Jacobian inverse
% to compensate for switch to primitive form
H1 = J_UI11.*G1 + J_UI12.*G2 + J_UI13.*G3;
H2 = J_UI21.*G1 + J_UI22.*G2 + J_UI23.*G3;
H3 = J_UI31.*G1 + J_UI32.*G2 + J_UI33.*G3;

% Add lower order terms
Drho = Drho + H1;
Dv = Dv + H2;
Dn = Dn + H3;

%%%%%%% SAT terms %%%%%%%%%%%%%%%%%

%%% Bottom boundary %%%

% Build rotation matrices
[R,Rinv,Lambda_plus] = rotationMatrix(rho(1),v(1),n(1)...
    ,M,'plus');

%%% WALL BC %%%
n_hat = n(1); rho_hat = rho(1); v_hat = -v(1);

% Specify BC in terms of primitive variables
g = [rho_hat; v_hat; n_hat];
V = [rho(1); v(1); n(1) ];

% Rotate to characteristic variables in SAT term
sat = -1*Hinv*R*Lambda_plus*Rinv*(V-g);

% Add SAT contributions
Drho(1) = Drho(1) + sat(1);
Dv(1) = Dv(1) + sat(2);
Dn(1) = Dn(1) + sat(3);

%%% Top boundary %%%

% Build rotation matrices
[R,Rinv,Lambda_minus] = rotationMatrix(rho(end),v(end),n(end)...
    ,M,'minus');

% Boundary data.
% OUTFLOW: Specify rho
% INFLOW:  Specify rho, n.
rho_hat = rho_boundary_data(M.p0/4,n_eq_func(M.p0/4,M),t,M);

if(v(end)<0)
    % This is an inflow
    n_hat = n(end); % SHOULD DO: n_hat = n_DATA;
    v_hat = v(end);

else
    % This is an outflow
    n_hat = n(end);
    v_hat = v(end);
end

% Specify BC in terms of primitive variables
g = [rho_hat; v_hat; n_hat];
V = [rho(end); v(end); n(end) ];

% Rotate to characteristic variables in SAT term
sat = 1*Hinv*R*Lambda_minus*Rinv*(V-g);

% Add SAT contributions
Drho(end) = Drho(end) + sat(1);
Dv(end) = Dv(end) + sat(2);
Dn(end) = Dn(end) + sat(3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function rho_data = rho_boundary_data(p0,n0,t,M)
    rho0 = rho_func(n0,p0,M);
    rho_data = rho0 + M.forcing.Arho*exp(-0.5*((t-M.forcing.t)/M.forcing.T)^2)*cos(2*pi*t/M.forcing.period); % add forcing
end

function mdot = mdot_func(rho,n,n_eq,M)
    mdot = -rho.*(n-n_eq)./M.tau;
end

function [G1, G2, G3] = G_func(mdot,rho,M)
    % Lower order terms in conservative formulation
    G1 = 0;
    G2 = rho*M.g; % + p*dA/dx + drag;
    G3 = mdot;
end

function [R,Rinv,Lambda_sign] = rotationMatrix(rho,v,n,M,sign)

    % Similar transformation R, and inv(R): Note that A=R*Lambda*RI
    k0 = M.k0;
    R = M.R; T = M.T;
    rho_l0 = M.rho_l0; p0 = M.p0;

    a = 1./(R.*T);
    b = rho_l0.*(1-p0./k0);
    c = rho_l0./k0;

    % Temporary variables in matrices
    % Speed of sound given by sqrt(M1)
    M1_plus = (0.1e1 ./ c ./ 0.2e1 + n ./ a ./ 0.2e1 - n ./ c ./ 0.2e1 + ((0.2e1 .* b ./ c - 0.2e1 .* rho ./ c - 0.2e1 .* rho .* n ./ a + 0.2e1 .* rho .* n ./ c) .* (-0.1e1 ./ c - n ./ a + n ./ c) ./ 0.4e1 + n .* b ./ c ./ a) .* ((b ./ c - rho ./ c - rho .* n ./ a + rho .* n ./ c) .^ 2 + 0.4e1 .* rho .* n .* b ./ a ./ c) .^ (-0.1e1 ./ 0.2e1));
    M2_plus = (rho ./ a ./ 0.2e1 - rho ./ c ./ 0.2e1 + ((0.2e1 .* b ./ c - 0.2e1 .* rho ./ c - 0.2e1 .* rho .* n ./ a + 0.2e1 .* rho .* n ./ c) .* (-rho ./ a + rho ./ c) ./ 0.4e1 + rho .* b ./ a ./ c) .* ((b ./ c - rho ./ c - rho .* n ./ a + rho .* n ./ c) .^ 2 + 0.4e1 .* rho .* n .* b ./ a ./ c) .^ (-0.1e1 ./ 0.2e1));

    M1_minus = 1./(2.*c)+(1./2).*n./a-(1./2).*n./c-(1./4).*((2.*(b./c-rho./c-rho.*n./a+rho.*n./c)).*(-1./c-n./a+n./c)+4.*n.*b./(c.*a))./sqrt((b./c-rho./c-rho.*n./a+rho.*n./c).^2+4.*rho.*n.*b./(a.*c));
    M2_minus = (1./2).*rho./a-(1./2).*rho./c-(1./4).*((2.*(b./c-rho./c-rho.*n./a+rho.*n./c)).*(-rho./a+rho./c)+4.*rho.*b./(a.*c))./sqrt((b./c-rho./c-rho.*n./a+rho.*n./c).^2+4.*rho.*n.*b./(a.*c));

    beta = n.*b.*rho./(a.*c);

    % If beta < 0, use "-sqrt".
    i1 = beta < 0;
    M1 = M1_plus; M1(i1) = M1_minus(i1);
    M2 = M2_plus; M2(i1) = M2_minus(i1);


    % Transformation matrices
    R = [(1 + M1 / rho .^ 2) .^ (-0.1e1 / 0.2e1) (1 + M1 / rho .^ 2) .^ (-0.1e1 / 0.2e1) -(M2 .^ 2 / M1 .^ 2 + 1) .^ (-0.1e1 / 0.2e1) * M2 / M1; ((1 + M1 / rho .^ 2) .^ (-0.1e1 / 0.2e1)) * sqrt(M1) / rho -((1 + M1 / rho .^ 2) .^ (-0.1e1 / 0.2e1)) * sqrt(M1) / rho 0; 0 0 (M2 .^ 2 / M1 .^ 2 + 1) .^ (-0.1e1 / 0.2e1);];
    Rinv = [((rho .^ 2 + M1) / rho .^ 2) .^ (-0.1e1 / 0.2e1) / rho .^ 2 * (rho .^ 2 + M1) / 0.2e1 ((rho .^ 2 + M1) / rho .^ 2) .^ (-0.1e1 / 0.2e1) / rho * M1 .^ (-0.1e1 / 0.2e1) * (rho .^ 2 + M1) / 0.2e1 M2 / M1 * ((rho .^ 2 + M1) / rho .^ 2) .^ (-0.1e1 / 0.2e1) / rho .^ 2 * (rho .^ 2 + M1) / 0.2e1; ((rho .^ 2 + M1) / rho .^ 2) .^ (-0.1e1 / 0.2e1) / rho .^ 2 * (rho .^ 2 + M1) / 0.2e1 -((rho .^ 2 + M1) / rho .^ 2) .^ (-0.1e1 / 0.2e1) / rho * M1 .^ (-0.1e1 / 0.2e1) * (rho .^ 2 + M1) / 0.2e1 M2 / M1 * ((rho .^ 2 + M1) / rho .^ 2) .^ (-0.1e1 / 0.2e1) / rho .^ 2 * (rho .^ 2 + M1) / 0.2e1; 0 0 sqrt((M1 .^ 2 + M2 .^ 2) / M1 .^ 2);];

    %Eigenvalues
    Lambda = [v + sqrt(M1) 0 0; 0 v - sqrt(M1) 0; 0 0 v;];

    if(strcmp(sign,'minus'))
        Lambda_sign = 1/2*(Lambda-abs(Lambda));
    else
        Lambda_sign = 1/2*(Lambda+abs(Lambda));
    end

end


end
