function [Dq1, Dq2, Dq3]=RHS_cons(t,q1,q2,q3,M,OPERATORS,RUSSIAN)
% RHS in PDE, conservative form

D = OPERATORS.D; Hinv = OPERATORS.Hinv;

% Compute needed quantities
rho = q1; v = q2./q1; n = q3./q1;
p = p_func(rho,n,M);
n_eq = n_eq_func(p,M);
mdot = mdot_func(rho,n,n_eq,M);

% Build flux
F1 = rho.*v; F2 = rho.*v.^2 + p; F3 = rho.*n.*v;

% Differentiate
F1x = D*F1; F2x = D*F2; F3x = D*F3;

% Derivative terms
Dq1 = -F1x;
Dq2 = -F2x;
Dq3 = -F3x;

if(RUSSIAN)
    % Russian dissipation!
    DISS = OPERATORS.DISS;
    alpha = maxEig(rho,v,n,M);
    Dq1 = Dq1 + alpha*DISS*q1;
    Dq2 = Dq2 + alpha*DISS*q2;
    Dq3 = Dq3 + alpha*DISS*q3;
else
    %%%%%%%%%%%%%%%% Ordinary AD %%%%%%%%%%%%%%%%%%%%%%%%%%
    ADO = OPERATORS.ADO;
    ADO_boundary = OPERATORS.ADO_boundary;
    C_AD = OPERATORS.C_AD;
    C_AD_boundary = OPERATORS.C_AD_boundary;
    Dq1 = Dq1 - (C_AD(1)*ADO + C_AD_boundary(1)*ADO_boundary)*q1;
    Dq2 = Dq2 - (C_AD(2)*ADO + C_AD_boundary(2)*ADO_boundary)*q2;
    Dq3 = Dq3 - (C_AD(3)*ADO + C_AD_boundary(3)*ADO_boundary)*q3;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

% Lower order terms in conservative formulation
[G1, G2, G3] = G_func(mdot,rho,v,p,n,M,OPERATORS);

% Add lower order terms
Dq1 = Dq1 + G1;
Dq2 = Dq2 + G2;
Dq3 = Dq3 + G3;

%%%%%%% SAT terms %%%%%%%%%%%%%%%%%

%======= Bottom boundary =================%
SAT = BC_bottom_cons(t,rho,v,n,q1,q2,q3,M,Hinv);

% Add SAT contributions
Dq1(1) = Dq1(1) + SAT(1);
Dq2(1) = Dq2(1) + SAT(2);
Dq3(1) = Dq3(1) + SAT(3);
%=========================================%

%======= Top boundary =================%
SAT = BC_top_cons(t,rho,v,n,q1,q2,q3,M,Hinv);

% Add SAT contributions
Dq1(end) = Dq1(end) + SAT(1);
Dq2(end) = Dq2(end) + SAT(2);
Dq3(end) = Dq3(end) + SAT(3);
%=========================================%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

function mdot = mdot_func(rho,n,n_eq,M)
    mdot = -rho.*(n-n_eq)./M.tau;
end

function [G1, G2, G3] = G_func(mdot,rho,v,p,n,M,OPERATORS)
    % Lower order terms in conservative formulation
    G1 = 0;
    G2 = -rho*M.g; % + p*dA/dx + drag;
    G3 = mdot;

    % Damping when driving towards steady state.
    D1 = OPERATORS.D; Hinv = OPERATORS.Hinvfull;
    D2 = OPERATORS.D2;
    D3 = OPERATORS.D3;
    G2 = G2 - M.alpha0*v;
    G2 = G2 - M.alpha1*Hinv*M.dx^2*(D1'*D1)*v;
    if(M.alpha2~=0); G2 = G2 - M.alpha2*Hinv*(D2'*D2)*v; end;
    if(M.alpha3~=0); G2 = G2 - M.alpha3*Hinv*(D3'*D3)*v; end;

    % drag

    % first convert mass fraction into volume fraction
    rhog = p/(M.R*M.T); % gas density
    phi = rho.*n./rhog; % gas volume fraction

    % drag depends on volume fraction

    % turbulent contribution
    drag = M.f0.*rho.*v.^2/(4*M.radius);

    % below fragmentation depth, mostly liquid
    % add laminar flow contribution (typically dominates turbulent)
    drag(phi<M.phi0) = drag(phi<M.phi0) + 16*M.mu*v(phi<M.phi0)/M.radius^2;

    G2 = G2 - drag;

end
