function n = n_func(rho,p,M)
    % Equation of state solved for n.
    
    R = M.R; T = M.T; rho_l0 = M.rho_l0; p0 = M.p0; k0 = M.k0;
    
    % Introduce the following constants:
    a = 1./(R.*T);
    b = rho_l0.*(1-p0./k0);
    c = rho_l0./k0;
    
    n = (a.*p.*(b+c.*p) -rho.*a.*p )./(rho.*(b+c.*p) -rho.*a.*p);
    
    n = max(n,0);

end