function p = p_func(rho,n,M)
% Equation of state solved for p.
    
    n = max(n,0);

    % Introduce the following constants:
    a = 1./(M.R.*M.T);
    b = M.rho_l0.*(1-M.p0./M.k0);
    c = M.rho_l0./M.k0;
    alpha = b./c -(n./a).*rho - rho.*(1-n)./c;
    beta = (n./c).*b.*rho./a;
    
    
    p1 = -alpha/2 + sqrt(1/4*alpha.^2 + beta);
%     p2 = -alpha/2 - sqrt(1/4*alpha.^2 + beta);
    
    p = p1;
    
end