rmpath([ pwd '/TimeStepping']);
rmpath([ pwd '/SBPoperators']);
rmpath([ pwd '/EquationOfState']);
rmpath([ pwd '/Parameters']);
rmpath([ pwd '/RHS']);
rmpath([ pwd '/BC']);
rmpath([ pwd '/FluxesAndJacobians']);
rmpath([ pwd '/InitialData']);