function DD = dissipationOperators(N,order)

switch order
    case 2
        DD = zeros(N+1,N+1);
        d2 = [1 -2 1];
        DD(1,1:3) = d2;
        DD(2,1:3) = d2;
        for i=3:(N-1)
            DD(i,(i-1):(i+1)) = d2;
        end
        DD(N  ,(N-1):(N+1)) = d2;
        DD(N+1,(N-1):(N+1)) = d2;
        
    case 3
        DD = zeros(N+1,N+1);
        d3 = [-1 3 -3 1];
        DD(1,1:4) = d3;
        DD(2,1:4) = d3;
        DD(3,1:4) = d3;
        for i=4:(N-2)
            DD(i,(i-2):(i+1)) = d3;
        end
        DD(N-1,(N-2):(N+1)) = d3;
        DD(N  ,(N-2):(N+1)) = d3;
        DD(N+1,(N-2):(N+1)) = d3;
end

DD = sparse(DD);