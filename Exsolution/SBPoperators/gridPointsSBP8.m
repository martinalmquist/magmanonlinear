function [x,dx] = gridPointsSBP8(N,L)

x1 = 3.8118550247622e-01; x2 = 1.1899550868338e+00;
x3 = 2.2476300175641e+00; x4 = 3.3192851303204e+00;

h = L/(N-8+2*x4); dx = h;

x = [0,h*x1,h*x2,h*x3,h*x4,...
    linspace(h*x4+h,L-(h*x4+h),N-9),...
    L - h*x4,L - h*x3,L - h*x2,L - h*x1,L]';