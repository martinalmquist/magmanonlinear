function M = steadyStateParameters(M)

M.tol = 1e0; % Tolerance when solving for steady state.
M.alpha0 = 0.5*1e4; % Damping parameter, -alpha0*v term;
M.alpha1 = 0*0.5*1e4; % Damping parameter, -alpha1*v_xx term;
M.alpha2 = 0*0.1*1e4; % Damping parameter, -alpha2*v_xxxx term;
M.alpha3 = 0*0.1*1e4; % Damping parameter, -alpha3*v_xxxxxx term;


M.BC_method_top = 'new';
M.BC_method_bottom = 'new';

switch M.p_ref_pos
    case 'Top'
        M.BC_top = 'p_n';
        M.p_data_top = @(t) M.p_ref;
        M.n_data_top = @(t) n_eq_func(M.p_ref,M);
        
        M.BC_bottom = 'wall';
        
    case 'Bottom'
        M.BC_bottom = 'p_n';
        M.p_data_bottom = @(t) M.p_ref;
        M.n_data_bottom = @(t) n_eq_func(M.p_ref,M);
        
        M.BC_top = 'wall';
end