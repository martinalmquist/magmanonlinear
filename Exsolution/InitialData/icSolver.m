function [rho,v,n,p] = icSolver(M)

x = M.x;
p_ref = M.p_ref;

%Numeric integration using ode45.
f = @(x,p) -M.g*rho_eq_func(p,M);

options = odeset('RelTol',1e-10,'AbsTol',1e-10);

switch M.p_ref_pos
    case 'Top'
        sol = ode45(f,flipud(x),p_ref,options);
    case 'Bottom'
        sol = ode45(f,x,p_ref,options);
end

p=deval(sol,x)';
n = n_eq_func(p,M);
rho = rho_func(n,p,M);
v = 0*x;

