function rho = rho_eq_func(p,M)
    % Equation of state solved for rho, at equilibrium
    
    n = n_eq_func(p,M);
    
    R = M.R; T = M.T; rho_l0 = M.rho_l0; p0 = M.p0; k0 = M.k0;
    
    % Introduce the following constants:
    a = 1./(R.*T);
    b = rho_l0.*(1-p0./k0);
    c = rho_l0./k0;
    
    rho = a.*p.*(b+c.*p)./(n.*(b+c.*p) + (1-n).*a.*p);

end