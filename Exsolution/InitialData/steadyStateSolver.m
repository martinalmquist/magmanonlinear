function [rho,v,n,p] = steadyStateSolver(M,rho,v,n,~,OPERATORS,UPWIND,FORM,RUSSIAN)

x = M.x;
dx = M.dx;
plot_solution = true;

% Runge-Kutta coefficients (4th order, low storage method)
RK = RKLS4();

% Conservative variables
% q1 = rho
% q2 = rho*v
% q3 = rho*n

% rates arrays
Dq1 = zeros(M.N+1,1); % dq1/dt
Dq2 = zeros(M.N+1,1); % dq2/dt
Dq3 = zeros(M.N+1,1); % dq3/dt

%%% Initial data and boundary conditions. %%%
M = steadyStateParameters(M);

% fields arrays
q1=rho; q2=rho.*v; q3=rho.*n;

%%% Time step %%%
CFL = M.CFL;
lmax = maxEig(rho,v,n,M);
dt = CFL*M.dx/lmax;

% loop over time steps
t = 0; % start at t=0
nt = 0; % number of time steps taken

% Setup plot
fh = figure();
fig_handles = plotSolution(fh,x,t,M,q1,q2,q3,FORM);

tol = M.tol; delta_q = 2*tol;
while delta_q > tol;

  [t,q1_new,q2_new,q3_new,Dq1,Dq2,Dq3] = RKLSstep(RK,q1,q2,q3,t,dt,Dq1,Dq2,Dq3,M,OPERATORS,UPWIND,FORM,RUSSIAN);
  delta_q = norm(q1_new-q1) + norm(q2_new-q2) + norm(q3_new-q3);
  q1 = q1_new; q2 = q2_new; q3 = q3_new;

  rho = q1;
  v = q2./q1;
  n = q3./q1;
  p = p_func(rho,n,M);

  % Adaptive time step
  lmax = maxEig(rho,v,n,M);
  dt = CFL*dx/lmax;

  %%% PLOTTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if plot_solution
    if mod(nt,20)==0;
        c = sound_speed(rho,n,M);
        updatePlot(fig_handles,rho,v,n,p,c,t,M);
    end
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  nt = nt +1;

end

rho = q1;
v = q2./q1;
n = q3./q1;
p = p_func(rho,n,M);