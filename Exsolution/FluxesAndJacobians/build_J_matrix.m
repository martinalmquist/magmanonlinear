function [B11, B12, B13,...
          B21, B22, B23,...
          B31, B32, B33] = build_J_matrix(rho,v,n,M)
%J
%    J = J(A,B,C,N,RHO,V)

%    This function was generated by the Symbolic Math Toolbox version 6.1.
%    12-Jan-2015 12:55:17

% Matrices involved in primitive form, conservative variables
% u_t+A*u_x=RHS, where u.^T=[rho rho*v rho*n]

k0 = M.k0;
R = M.R; T = M.T;
rho0 = M.rho_l0; p0 = M.p0; 

a = 1./(R.*T);
b = rho0.*(1-p0./k0);
c = rho0./k0;

t2 = 1.0./c;
t4 = 1.0./a;
t5 = n.*rho;
t6 = b-rho+t5;
t7 = t2.*t6;
t8 = n.*rho.*t4;
t3 = t7-t8;
t9 = t3.^2;
t10 = b.*n.*rho.*t2.*t4.*4.0;
t11 = t9+t10;
t12 = 1.0./sqrt(t11);
t13 = t2.*(1.0./2.0);

B11 = 0.0;
B21 = t13-v.^2-t2.*t3.*t12.*(1.0./2.0);
B31 = -n.*v;
B12 = 1.0;
B22 = v.*2.0;
B32 = n;
B13 = 0.0;
B23 = -t13+t4.*(a.*t12.*(t3.*(t2-t4).*2.0+b.*t2.*t4.*4.0).*(1.0./4.0)+1.0./2.0);
B33 = v;

