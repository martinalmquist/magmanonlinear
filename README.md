# README #

### What is this repository for? ###

Nonlinear magma flow. The folder 'Exsolution' contains the main code. The folder 'NoExsolution' contains the simplified problem in which the gas mass fraction n is identically 0.

### Major updates ###
* Nov 3 2015: Gravity is included. The main program automatically solves an ODE to
determine equilibrium initial data.
* Nov 6 2015: [Upwind difference operators by Ken Mattsson](https://doi.org/10.1016/j.jcp.2017.01.042) are now used by default.

### User guidelines ###

Right now there are two .m-files corresponding to two test problems, namely:

* **Problem 1**: eruption triggered by a pressure drop at the surface. Implemented in pressureDropSurface.m
* **Problem 2**: eruption triggered by a pressure increase at the bottom. Implemented in pressureIncreaseBottom.m

To play around with a new problem, just copy one of the above files and modify initial and boundary data, and type of boundary conditions, as required. Then run “main” with a handle to the function in which the problem is defined. For example, you can test Problem 1 with 256+1 grid points by running
`main(256,[],[],@pressureDropSurface);`
The optimized 8th order upwind SBP operator is used by default. With the current parameter set, the top boundary quickly becomes a supersonic outflow.

To test problem 2:
`main(256,[],[],@pressureIncreaseBottom);`

If you don't want to use the upwind difference operators, you can run
`main(256,[],[],@pressureIncreaseBottom,[],[],[],false);`
and you can switch on artificial dissipation:
`main(256,[],[],@pressureIncreaseBottom,true,true,[],false);`
With the artificial dissipation, the scheme is quite robust.

You might also want to have a look in problemParameters.m, where temperature, gas constants etc. are set, but the values there should be reasonable.

### Who do I talk to? ###

* For numerics, contact Martin. martin.almquist@it.uu.se.
