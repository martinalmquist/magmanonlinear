function plotEqSol(N)
SUBFOLDERS;
close all;

FORM = 'cons';
L = 3000;
x = linspace(0,L,N+1)';

% Problem parameters stored in struct
M = problemParameters(x,L);
M.p_ref = M.pa; 
M.p_ref_pos = 'Top';

[rho,v,n] = icSolver(M);

t = 0;
q1 = rho; q2 = rho.*v; q3 = rho.*n;
plotIC(x,t,M,q1,q2,q3,FORM);
