function [rho,v,p] = icSolver(M)

x = M.x;
p_ref = M.p_ref;

%Numeric integration using ode45.
f = @(x,p) -M.g*rho_func(p,M);

options = odeset('RelTol',1e-8,'AbsTol',1e-8);

switch M.p_ref_pos
    case 'Top'
        sol = ode45(f,[x(end) x(1)],p_ref,options);
    case 'Bottom'
        sol = ode45(f,[x(1) x(end)],p_ref,options);
end

p=deval(sol,x)';
rho = rho_func(p,M);
v = 0*x;

