function plotIC(x,t,M,q1,q2,FORM)

if( strcmp(FORM,'cons') || strcmp(FORM,'prim') )
    rho = q1; v = q2./q1;
else
    rho = q1; v = q2;
end
p = p_func(rho,M);

%%% SUBPLOTS %%%
subplot(3,1,1)
plot(x,p,'b-o')
ylabel('p')

subplot(3,1,2)
plot(x,rho,'b-o')
ylabel('rho')

subplot(3,1,3)
plot(x,v,'b-o');
ylabel('v')

title(['t = ' num2str(t)])

drawnow;