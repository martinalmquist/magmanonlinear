function B = createDampingCoefficient(m,L)
% Smooth function used as damping coefficient
% B is zero at the boundaries and 1 in the interior

%%%%%%%% Grid point distribution %%%%%%%%%%%%%%%%%%%%%%
d1 =  3.8118550247622e-01;
d2 =  8.0876958435753e-01;
d3 =  1.0576749307304e+00;
d4 =  1.0716551127563e+00;

dd=d1+d2+d3+d4;

x_l = 0;
h=L/(2*dd+(m-9));

x = ones(m,1)*(x_l+dd*h)+h*[-dd, -(d2+d3+d4),-(d3+d4), -d4, linspace(0,m-9,m-8), (m-9+d4),(m-9+d4+d3) , (m-9+d2+d3+d4), (m-9+dd)]';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N_min=8; % Minimum number of points on the transition from 0 to 1.
AREA_target = 0.05; % Targeted ratio (transition area) / (whole domain).

% Find x closest to AREA_target
[~,index] = min( abs(x-AREA_target*L) );
% Check that we use at least N_min points
if(index < N_min); 
    xr = x(N_min);
    N = N_min;
else
    xr = x(index);
    N = index;
end

x0=0; x1 = xr;
d=0; % Start level
g=1; % Interior level

c0 = (-10*x1^3*x0^2*d+5*x1^4*x0*d-d*x1^5+10*x1^2*x0^3*g-5*x1*x0^4*g+x0^5* ...
      g)/(10*x1^2*x0^3-10*x0^2*x1^3+5*x0*x1^4-5*x1*x0^4+x0^5-x1^5);
c1 = -30*(-d+g)*x1^2*x0^2/(10*x1^2*x0^3-10*x0^2*x1^3+5*x0*x1^4-5*x1*x0^4+x0^5-x1^5);
c2 = 30*(-d+g)*x1*x0*(x0+x1)/(10*x1^2*x0^3-10*x0^2*x1^3+5*x0*x1^4-5*x1* ...
			      x0^4+x0^5-x1^5);
c3 = -10*(-d+g)*(x0^2+4*x1*x0+x1^2)/(10*x1^2*x0^3-10*x0^2*x1^3+5*x0*x1^4- ...
				     5*x1*x0^4+x0^5-x1^5);
c4 = 15*(-d+g)*(x0+x1)/(10*x1^2*x0^3-10*x0^2*x1^3+5*x0*x1^4-5*x1*x0^4+ ...
			x0^5-x1^5);
c5 = -6*(-d+g)/(10*x1^2*x0^3-10*x0^2*x1^3+5*x0*x1^4-5*x1*x0^4+x0^5-x1^5);

xt=x(1:N);
cx=polyval([c5 c4 c3 c2 c1 c0],xt);

%%%% Higher order polynomial %%%%
% c0 = 0; c1 = 0; c2 = 0; c3 = 0; c4 = 0; c5 = 126/x1^5; c6 = -420/x1^6;
% c7 = 540/x1^7; c8 = -315/x1^8; c9 = 70/x1^9;
% cx=polyval([c9 c8 c7 c6 c5 c4 c3 c2 c1 c0],x(1:N));

B=eye(m);
B(1:N,1:N)=diag(cx);
B(m-N+1:m,m-N+1:m)=diag(flipud(cx));
