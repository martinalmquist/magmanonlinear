function [Dq1, Dq2]=RHS_prim(~,t,q1,q2,M,Hinv,D,~,~,ADO,ADO_boundary,C_AD,C_AD_boundary)
% RHS in PDE, primitive form
    
% Compute needed quantities
rho = q1; v = q2./q1;
p = p_func(rho,M);

% Build matrix B (Flux Jacobian)
[B11, B12, ...
 B21, B22, ...
 ] = build_J_matrix(rho,v,M);

% Differentiate
q1_x = D*q1; q2_x = D*q2;

% Derivative terms
Dq1 = -B11.*q1_x - B12.*q2_x;
Dq2 = -B21.*q1_x - B22.*q2_x;

%%%%%%%%%%%%%%%% Ordinary AD %%%%%%%%%%%%%%%%%%%%%%%%%%
Dq1 = Dq1 - (C_AD(1)*ADO + C_AD_boundary(1)*ADO_boundary)*q1;
Dq2 = Dq2 - (C_AD(2)*ADO + C_AD_boundary(2)*ADO_boundary)*q2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Lower order terms in conservative formulation
[G1, G2] = G_func(rho,M);

% Add lower order terms
Dq1 = Dq1 + G1;
Dq2 = Dq2 + G2;

%%%%%%% SAT terms %%%%%%%%%%%%%%%%%

%======= Bottom boundary =================%
SAT = BC_bottom_cons(t,rho,v,q1,q2,M,Hinv);

% Add SAT contributions
Dq1(1) = Dq1(1) + SAT(1);
Dq2(1) = Dq2(1) + SAT(2);
%=========================================%

%======= Top boundary =================%
SAT = BC_top_cons(t,rho,v,q1,q2,M,Hinv);

% Add SAT contributions
Dq1(end) = Dq1(end) + SAT(1);
Dq2(end) = Dq2(end) + SAT(2);
%=========================================%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

function [G1, G2] = G_func(rho,M)
    % Lower order terms in conservative formulation
    G1 = 0;
    G2 = -rho*M.g; % + p*dA/dx + drag; 
end
