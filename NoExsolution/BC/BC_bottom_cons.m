function SAT = BC_bottom_cons(t,rho,v,n,q1,q2,M,Hinv)
% Boundary conditions for the bottom boundary, conservative variables.


% Build rotation matrices
[R,Rinv,Lambda_plus] = rotationMatrix_J(v(1)...
    ,M,'plus');

R11 = Rinv(1,1); R12 = Rinv(1,2); 
R21 = Rinv(2,1); R22 = Rinv(2,2); 

R
Rinv
pause

%%%%%%%%%%%%%%%%%%%%%% OSSIAN'S NEW TECHNIQUE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-------- Data for rho and n. If outflow, only data for rho is used. ------------------%
if( strcmp(M.BC_bottom,'rho_n') && strcmp(M.BC_method_bottom,'new') )
    rho_data = M.rho_data_bottom(t);
    n_data = M.n_data_bottom(t);
    V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
    if(v(1)>0)
        % This is an inflow
        % w2 is outgoing
        w2 = R21*rho(1) + R22*rho(1)*v(1) + R23*rho(1)*n(1);
        w1 = (R11-R21*R12/R22)*rho_data + (R13-R23*R12/R22)*rho_data*n_data + R12/R22*w2;
        w3 = (R31-R21*R32/R22)*rho_data + (R33-R23*R32/R22)*rho_data*n_data + R32/R22*w2;
        
        % Rotate to characteristic variables in SAT term
        V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
        W = [w1;w2;w3];
        SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    else
        % This is an outflow. Ignore data for n.
        % w3 is ingoing
        W = Rinv*V;
        w1 = W(1); w2 = W(2);
        a_vec = R'*[1;0;0]; a1 = a_vec(1); a2 = a_vec(2); a3 = a_vec(3);
        w3 = 1/a3*(rho_data - a1*w1 - a2*w2);
        
        % Rotate to characteristic variables in SAT term
        W = [w1;w2;w3];
        SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    end
end
%--------------------------------------------------------%

%-------- Data for p and n. If outflow, only data for p is used. ------------------%
if( strcmp(M.BC_bottom,'p_n') && strcmp(M.BC_method_bottom,'new') )
    p_data = M.p_data_bottom(t);
    n_data = M.n_data_bottom(t);
    rho_data = rho_func(n_data,p_data,M); % Eq of state
    V = [rho(1); rho(1)*v(1); rho(1)*n(1)];
    if(v(1)>0)
        % This is an inflow
        % w2 is outgoing
        w2 = R21*rho(1) + R22*rho(1)*v(1) + R23*rho(1)*n(1);
        w1 = (R11-R21*R12/R22)*rho_data + (R13-R23*R12/R22)*rho_data*n_data + R12/R22*w2;
        w3 = (R31-R21*R32/R22)*rho_data + (R33-R23*R32/R22)*rho_data*n_data + R32/R22*w2;
        
        % Rotate to characteristic variables in SAT term
        V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
        W = [w1;w2;w3];
        SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    else
        % This is an outflow. Ignore data for n.
        n_data = n(1);
        rho_data = rho_func(n_data,p_data,M); % Eq of state
        
        % w3 is ingoing
        W = Rinv*V;
        w1 = W(1); w2 = W(2);
        a_vec = R'*[1;0;0]; a1 = a_vec(1); a2 = a_vec(2); a3 = a_vec(3);
        w3 = 1/a3*(rho_data - a1*w1 - a2*w2);
        
        % Rotate to characteristic variables in SAT term
        W = [w1;w2;w3];
        SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    end
end
%--------------------------------------------------------%

%-------- Wall boundary, velocity = 0. ------------------%
% Martin is not 100% sure about what we can do with w2, which 
% is moving with speed 0, but Ossian is confident that we can set
% w3 = a*w1 + b*w2 anyway.
if( strcmp(M.BC_bottom,'wall') && strcmp(M.BC_method_bottom,'new') )
    % w3 is ingoing
    % w2 is outgoing
    % w1 has speed 0.
    
    v_data = 0;
    
    V = [rho(1); rho(1)*v(1); rho(1)*n(1)];
    W = Rinv*V;
    w1 = W(1); w2 = W(2);
    a_vec = R'*[0;1/rho(1);0]; a1 = a_vec(1); a2 = a_vec(2); a3 = a_vec(3);
    w3 = 1/a3*(v_data - a1*w1 - a2*w2);

    % Rotate to characteristic variables in SAT term
    W = [w1;w2;w3];
    SAT = -1*Hinv*R*Lambda_plus*(Rinv*V-W);
    
end
%--------------------------------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%% KEN'S AND MARTINS'S OLD TECHNIQUE %%%%%%%%%%%%%%%%%%
%-------- Data for rho and n. If outflow, only data for rho is used. ------------------%
if( strcmp(M.BC_bottom,'rho_n') && strcmp(M.BC_method_bottom,'old') )
    rho_data = M.rho_data_bottom(t);
    n_data = M.n_data_bottom(t);
    v_data = v(1);
    if(v(1)>0)
        % This is an inflow
    else
        % This is an outflow. Ignore data for n.
        n_data = n(1);
    end
    
    % Rotate to characteristic variables in SAT term
    V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
    g = [rho_data;rho_data*v_data;rho_data*n_data];
    SAT = -1*Hinv*R*Lambda_plus*Rinv*(V-g);
end
%--------------------------------------------------------%

%-------- Data for p and n. If outflow, only data for p is used. ------------------%
if( strcmp(M.BC_bottom,'p_n') && strcmp(M.BC_method_bottom,'old') )
    p_data = M.p_data_bottom(t);
    n_data = M.n_data_bottom(t);
    rho_data = rho_func(n_data,p_data,M);
    v_data = v(1);
    if(v(1)>0)
        % This is an inflow
    else
        % This is an outflow. Ignore data for n.
        n_data = n(1);
        rho_data = rho_func(n_data,p_data,M);
    end
    
    % Rotate to characteristic variables in SAT term
    V = [rho(1); rho(1)*v(1); rho(1)*n(1) ];
    g = [rho_data;rho_data*v_data;rho_data*n_data];
    SAT = -1*Hinv*R*Lambda_plus*Rinv*(V-g);
end
%--------------------------------------------------------%

%-------- Wall boundary, velocity = 0. ------------------%
if( strcmp(M.BC_bottom,'wall') && strcmp(M.BC_method_bottom,'old') )
    
    n_hat = n(1); rho_hat = rho(1); v_hat = -v(1);
    
    % Specify BC in terms of conservative variables
    q1_BC = rho_hat; q2_BC = rho_hat*v_hat; q3_BC = rho_hat*n_hat;
    g = [q1_BC; q2_BC; q3_BC];
    V = [q1(1); q2(1); q3(1) ];
    
    % Rotate to characteristic variables in SAT term
    SAT = -1*Hinv*R*Lambda_plus*Rinv*(V-g);
    
end
%--------------------------------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Rotation matrices from Maple.
function [R,Rinv,Lambda_sign] = rotationMatrix_J(v,M,sign)
        
        c = M.c;
    
        % Similar transformation R, and inv(R): Note that A=R*Lambda*RI
        
        R = [1, 1;...
            v+c, v-c];
        
        Rinv = [(c-v)/(2*c)];
        
        if(strcmp(sign,'minus'))
            Lambda_sign = 1/2*(Lambda-abs(Lambda));
        else
            Lambda_sign = 1/2*(Lambda+abs(Lambda));
        end
        
    end


end