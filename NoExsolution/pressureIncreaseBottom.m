function M = pressureIncreaseBottom(M,p,rho,v,n)
    
    if(nargin <= 1 )
        % Initial pressure is known at either bottom or top.
        % This is used to solve an IVP for the initial equilibrium
        % state
        M.p_ref = 1e7; M.p_ref_pos = 'Top';
    end
    
    if(nargin > 1 )
        
        M.p_initial = p;
        M.v_initial = v;
        M.rho_initial = rho;
        M.n_initial = n;
        
        %%%%%%% Bottom boundary %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % M.BC_bottom = 'wall'; % Wall boundary, v = 0;
        % M.BC_bottom = 'rho_n'; % Data for rho and n. If outflow, data for n is ignored.
        M.BC_bottom = 'p_n'; % Data for p and n. If outflow, data for n is ignored.

        M.BC_method_bottom = 'new';
        % 'new' is "Ossian's way", which works very well and is recommended.
        % 'old' is the way Ken and Martin used to do it, which doesn't quite work
        % in some cases.

        %======= Boundary data ==============%
        % rho
        rho0 = M.rho_initial;
        M.rho_data_bottom = @(t) rho0(1); % Constant density.

        % p
%         pb = 2e7; % Chamber pressure
        p0 = M.p_initial(1);
        pb = p0 + 0*1e6;
        tau = 1e-1; t0 = 5;
        M.p_data_bottom = transition(p0,pb,tau,t0);
        % Pressure increase at time t0, time scale tau.
        figure; fplot(M.p_data_bottom,[0,10])
        xlabel('Time (s)'); ylabel('Pressure (Pa)');
        title('Pressure data for bottom boundary')
%         pause(2)

        %n
        M.n_data_bottom = @(t) n_eq_func(M.p_data_bottom(t),M); % Equilibrium gas fraction
        %====================================%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%% TOP boundary %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % M.BC_top = 'wall'; % Wall boundary, v = 0;
        % M.BC_top = 'rho_n'; % Data for rho and n. If outflow, data for n is ignored.
        M.BC_top = 'p_n'; % Data for p and n. If outflow, data for n is ignored.

        M.BC_method_top = 'new';

        %======= Boundary data ==============%

        % rho
        rho0 = M.rho_initial;
        M.rho_data_top = @(t) rho0(end); % Constant density.

        % n
        n0 = M.n_initial(end);
        M.n_data_top = @(t) n0; % Constant n

        % p
        p0 = M.p_initial(end);
        M.p_data_top = @(t) p0; % Constant pressure.
        %====================================%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
end

function func_handle = transition(start,stop,time_scale,t0)
    
    func_handle = @(t) start + ...
        (stop-start)*(1/2+1/pi*atan((t-t0)/time_scale));
    
end