function Lambda_max = maxEig(v,M)

    %Eigenvalues
    Lambda_max = max(max([abs(v + M.c); abs(v - M.c)]));
    
    
end