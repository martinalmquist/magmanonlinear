function [t,q1,q2,Dq1,Dq2] = RKLSstep(RK,q1,q2,t,dt,Dq1,Dq2,M,OPERATORS,UPWIND,FORM)
% Take one time step with the low-storage Runge-Kutta method defined by the
% parameters in the struct RK.

t0 = t;

Dplus = OPERATORS.Dplus; Dminus = OPERATORS.Dminus;
D = OPERATORS.D; Hinv = OPERATORS.Hinv;
ADO = OPERATORS.ADO; ADO_boundary = OPERATORS.ADO_boundary;
C_AD = OPERATORS.C_AD; C_AD_boundary = OPERATORS.C_AD_boundary;

for k=1:RK.nstage
    
    % time
    t = t0+RK.C(k)*dt;
    
    % scale rates
    Dq1 = RK.A(k)*Dq1; Dq2 = RK.A(k)*Dq2; Dq3 = RK.A(k)*Dq3;
    
    % set rates
    if( strcmp(FORM,'cons') )
        [Dq1_new, Dq2_new] = ...
            RHS_cons(t,q1,q2,M,Hinv,D,ADO,ADO_boundary,C_AD,C_AD_boundary);
    end
    if( strcmp(FORM,'prim') )
        [Dq1_new, Dq2_new] = ...
            RHS_prim(UPWIND,t,q1,q2,M,Hinv,D,Dplus,Dminus,ADO,ADO_boundary,C_AD,C_AD_boundary);
    end
    if( strcmp(FORM,'prim_rho_v_n') )
        [Dq1_new, Dq2_new] = ...
            RHS_prim_rho_v_n(UPWIND,t,q1,q2,M,Hinv,D,Dplus,Dminus,ADO,ADO_boundary,C_AD,C_AD_boundary);
    end
    
    % add rates to old rates
    Dq1 = Dq1+Dq1_new;
    Dq2 = Dq2+Dq2_new;
    
    % update fields
    q1 = q1+dt*RK.B(k)*Dq1;
    q2 = q2+dt*RK.B(k)*Dq2;
    
end

t = t0+dt;