addpath([ pwd '/TimeStepping']);
addpath([ pwd '/SBPoperators']);
addpath([ pwd '/EquationOfState']);
addpath([ pwd '/Parameters']);
addpath([ pwd '/RHS']);
addpath([ pwd '/BC']);
addpath([ pwd '/FluxesAndJacobians']);
addpath([ pwd '/InitialData']);