function [J_UI11, J_UI12, J_UI13,...
          J_UI21, J_UI22, J_UI23,...
          J_UI31, J_UI32, J_UI33] = build_Jacobian_inverse(rho,v,n)
    
    % Builds the inverse of the Jacobian J_U = dq/du,
    % where q are the conservative variables and u the primitive variables.
      
    J_UI11 = 1; J_UI12 = 0; J_UI13 = 0;
    J_UI21 = -v./rho; J_UI22 = 1./rho; J_UI23 = 0; 
    J_UI31 = -n./rho; J_UI32 = 0; J_UI33 = 1./rho;
    
      
end