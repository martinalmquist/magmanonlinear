function rho = rho_func(p,M)
    % Equation of state solved for rho.
    
    rho = p/M.c^2 + M.b;

end