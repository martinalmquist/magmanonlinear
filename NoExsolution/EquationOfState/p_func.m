function p = p_func(rho,M)
% Equation of state solved for p.
    
    p = M.c^2*(rho-M.b);
    
end