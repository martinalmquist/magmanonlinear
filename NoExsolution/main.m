% Main code for 1D nonlinear magma wave equation
% N = number of grid points
% order = internal order of accuracy
% plot_solution = true to plot solution at all time steps
%
% FORM ('cons' or 'prim')
% 'cons' - conservative formulation, variables q = [rho, rho*v, rho*n]';
% 'prim' - primitive (quasilinear) formulation, variables q = [rho, rho*v, rho*n]';
%
% UPWIND (true or false)
% For the two primitive formulations, artificial dissipation can be
% introduced via 'upwinding', i.e B*D*q is replaced by B_+*D_+*q + B_-*D_-*q. 
% The conservative formulation does not have the upwinding option.
%
% AD (true or false)
% Artificial dissipation of the form 
% q1_t = ... + Hinv*D_p^T*C1*D_p*q1;
% q2_t = ... + Hinv*D_p^T*C2*D_p*q2;
%
%
%
% out = data structure containing solution, parameters, etc.
%

function out=main(N,order,plot_solution,PROBLEM,UPWIND,AD,FORM)

% Add subfolders to MATLAB path.
SUBFOLDERS;
close all;
    
% Default options
if (nargin <= 6 || isempty(FORM)); FORM = 'prim'; end;
if (nargin <= 5 || isempty(AD)); AD = false; end;
if (nargin <= 4 || isempty(UPWIND)); UPWIND = false; end;
if (nargin <= 3 || isempty(PROBLEM)); PROBLEM = @pressureDropSurface; end;
if (nargin <= 2 || isempty(plot_solution)); plot_solution = true; end;
if (nargin == 1 || isempty(order)); order = 8; end;

% total simulation time
tmax = 100;

available_forms = {'prim'};
if(~strcmp(FORM,available_forms)); error('That form is not available'); end;

%%% Artificial damping parameters, tuned with tau = 0.1 %%%
[C_AD,C_AD_boundary,C_upwind,C_upwind_boundary]...
    = dissipationParameters(order,FORM,AD);

%%% CFL constant for time-stepping %%
CFL = CFLconstant(order);

% Special distribution of grid points for the 8th order operator
L = 3000; % Length of domain
if(order == 8)
    [x,dx] = gridPointsSBP8(N,L);
else 
    dx = L/N; % length of domain, grid spacing
    x = (0:dx:L)'; % coordinates (at N+1 grid points)
end

% Problem parameters stored in struct
M = problemParameters(x,L);

% operator splitting to handle stiffness from exsolution
% TO DO: Implement IMEX Runge-Kutta.
M.operator_splitting = false; % true or false

% SBP differentiation matrix
% D = first derivative
% Hinv = corner entry of diagonal norm (for SAT penalties)
% ADO = artificial dissipation operator
% order = interior order of accuracy
[Dplus,Dminus,ADO,ADO_boundary] = SBPoperators_upwind(N,dx,order,C_upwind,C_upwind_boundary);
[D,Hinv] = SBPoperators(N,dx,order);
OPERATORS = struct; 
OPERATORS.Dplus = Dplus; OPERATORS.Dminus = Dminus;
OPERATORS.D = D; OPERATORS.Hinv = Hinv;
OPERATORS.ADO = ADO; OPERATORS.ADO_boundary = ADO_boundary;
OPERATORS.C_AD = C_AD; OPERATORS.C_AD_boundary = C_AD_boundary;

% Runge-Kutta coefficients (4th order, low storage method)
RK = RKLS4();

% Conservative variables
% q1 = rho
% q2 = rho*v

% rates arrays
Dq1 = zeros(N+1,1); % dq1/dt
Dq2 = zeros(N+1,1); % dq2/dt

%%% Initial data and boundary conditions. %%%
M = PROBLEM(M);
% Solve ODE for equilibrium initial condition
[rho0,v0,p0] = icSolver(M);
M = PROBLEM(M,p0,rho0,v0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Time step %%%
lmax = maxEig(v0,M);
dt = CFL*dx/lmax;
nt = ceil(tmax/dt); % number of time steps (approximately, time step is adapted)
% dt = tmax/nt; % adjust dt to finish exactly at t=tmax

% fields arrays
q1=rho0; q2=rho0.*v0;

% storage arrays for all time steps 
% (There may be more/fewer time steps since the time step is adapted.)
out.rho = zeros(N+1,nt+1); out.v = zeros(N+1,nt+1);
out.p = zeros(N+1,nt+1);

% save initial conditions
out.rho(:,1) = rho0; out.v(:,1) = v0; out.p(:,1) = p0;

% save space and time vectors
out.t = (0:nt)*dt; out.x = x;

% loop over time steps
t = 0; % start at t=0
nt = 0; % number of time steps taken

plotSolution(x,t,M,q1,q2,FORM); pause;

while t < tmax;

  [t,q1,q2,Dq1,Dq2] = RKLSstep(RK,q1,q2,t,dt,Dq1,Dq2,M,OPERATORS,UPWIND,FORM);
  nt = nt+1;
  
  % save fields and pressure (use conservative variables if possible)
  rho = q1; v = q2./q1;
    
  out.rho(:,nt+1) = rho;
  out.v(:,nt+1) = v;
  out.p(:,nt+1) = p_func(rho,M);
  
  % Adaptive time step
  lmax = maxEig(v,M);
  dt = CFL*dx/lmax;
  if(t+dt>tmax); dt = tmax - t; end;
  
  %%% PLOTTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if plot_solution
    if mod(nt,1)==0; plotSolution(x,t,M,q1,q2,FORM); pause; end;
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end

% return other variables as well
out.M = M;
out.L = L;
out.N = N;
out.dt = dt;
out.nt = nt;