function [B11, B12,...
          B21, B22...
          ] = build_J_matrix(rho,v,M)

% Matrices involved in primitive form, conservative variables
% u_t+A*u_x=RHS, where u.^T=[rho rho*v]

B11 = zeros(size(rho));
B12 = ones(size(rho));
B21 = M.c^2 - v.^2;
B22 = 2*v;