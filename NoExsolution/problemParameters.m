function M = problemParameters(x,L)

% material properties (stored in data structure M)
%
% M.g = gravitational acceleration
% M.tau = gas exsolution time scale

M.g = 10;

M.tau = 1;
M.L = L;
M.x = x;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%% For n_eq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M.s = 4*1e-6; % sqrt(Pascal)
M.n0 = 0.01;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%% PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%
M.R = 461; M.T = 1200; M.rho_l0 = 2600;
% c_l = 2;
% M.k0 = c_l^2*M.rho_l0;
 M.p0 = M.n0^2/M.s^2;
%M.p0 = 10^8; 
M.k0 = 10*1e9;
M.pa = 101325; % Atmpospheric pressure
% M.s = M.n0/sqrt(M.p0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Constants:
M.b = M.rho_l0.*(1-M.p0./M.k0);
d = M.rho_l0./M.k0;

% Speed of sound
M.c = sqrt(1/d);