function plotSolution(x,t,M,q1,q2,q3,FORM)

if( strcmp(FORM,'cons') || strcmp(FORM,'prim') )
    rho = q1; v = q2./q1; n = q3./q1;
else
    rho = q1; v = q2; n = q3;
end
p = p_func(rho,n,M);

%%% SUBPLOTS %%%
subplot(4,1,1)
plot(x,p,'b-o')
ylabel('p')
hold on;
plot(x(1),M.p_data_bottom(t),'rx');
plot(x(end),M.p_data_top(t),'rx');
hold off;

subplot(4,1,2)
plot(x,n,'b-o')
ylabel('n')
hold on;
plot(x(1),M.n_data_bottom(t),'rx');
plot(x(end),M.n_data_top(t),'rx');
hold off;

subplot(4,1,3)
plot(x,rho,'b-o')
ylabel('rho')

subplot(4,1,4)
%       c_star = sound_speed(rho_prim,n_prim,M);
plot(x,v,'b-o');%,x,c_star,'r-x');
ylabel('v')

title(['t = ' num2str(t)])

drawnow;